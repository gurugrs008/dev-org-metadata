import { LightningElement } from 'lwc';

export default class TestLWC extends LightningElement {
	varname = 'Gaurav';
	changeHandler(event) {
		this.varname = event.target.value;
	}
}