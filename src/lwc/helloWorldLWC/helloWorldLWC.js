import { LightningElement } from 'lwc';

export default class HelloWorldLWC extends LightningElement {
	greeting = 'world';
	changeHandler(event) {
		this.greeting = event.target.value;
	}
}