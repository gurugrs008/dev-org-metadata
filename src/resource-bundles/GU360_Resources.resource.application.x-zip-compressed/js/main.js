$(document).ready(function() {
  // Turn on the fancyselect inputs
  //$('.fancyselect').fancyselect();

  // Set the current profile subnav page as active in the dropdown
  var dropdownTitle = $('.profile-subnav .dropdown-items a.active').text();
  $('.profile-subnav label .title').html(dropdownTitle);

  //mobile search toggle

  $(".mobile-search").click(function(){
    $(".site-search").toggleClass("active");
  });

});

// Open the dropdown when it's tabbed
$('.dropdown > label').keyup(function() {
  if ($(this).next('input').is(':checked')) {
    $(this).next('input').prop("checked", false);
  } else {
    $(this).next('input').prop("checked", true);
  }
});

// Force the dropdown to close when anything else is clicked
$(document).on('click', function(event) {
  if (!$(event.target).closest('.dropdown').length) {
    $('.dropdown input').prop("checked", false);
  }
});

// Faculty Profile header update block
$('#save-update-header').click(function () {
  var role01 = $('#role01input').val();
  var role02 = $('#role02input').val();
  $('#role01txt').text( role01 );
  $('#role02txt').text( role02 );
});

$(document).ready(function() {
  // All update blocks
  $('.update-block').click(function () {
    $(this).addClass('slds-hide');
    $(this).next('.edit').removeClass('slds-hide');
  });

  // Make edit-block show in the proper tab order / editable
  $('.edit-block').focus(function () {
    $(this).children('.update-block').addClass('slds-hide');
    $(this).children('.edit').removeClass('slds-hide');
  });

  $('.btn-update').click(function () {
    $(this).closest('.edit').addClass('slds-hide').prev('.read').removeClass('slds-hide');
  });

  // All list-card edit buttons
  $('.list-row .btn-edit').click(function () {
    $(this).parent().parent().next('.edit').removeClass('slds-hide');
    $(this).closest('.read').addClass('slds-hide');
  });

  $('.list-row .save-row .btn-bs').click(function () {
    $(this).parent().parent(".edit").addClass('slds-hide');
    $(this).parent().parent().prev(".read").removeClass('slds-hide');
  });

  // All add blocks
  $('.btn-add').click(function () {
    $(this).parent().parent().next('.add-block').removeClass('slds-hide');
  });

  $('.btn-add-control').click(function () {
    $(this).closest('.add-block').addClass('slds-hide');
    //$(this).closest('.read').removeClass('slds-hide');
  });

  // Clear text input's
  $('.btn-clear').click(function () {
    $(this).next('input').val('');
  });

  // Single input edit block
  $('.control-input .slds-input').click(function () {
    $(this).addClass('unsaved');
  });
  $('.control-input .input-save').click(function () {
    $(this).parent().prev('.slds-input').removeClass('unsaved');
  });
  $('.control-input .input-cancel').click(function () {
    $(this).parent().prev('.slds-input').removeClass('unsaved');
  });
  $('.control-input .slds-input').focus(function () {
    $(this).addClass('unsaved');
  });
  $('.touch-input').focus(function () {
    $(this).addClass('unsaved');
    $('.slds-select').focus();
  });

  // Single input select block
  $('.control-input .value').click(function () {
    $(this).parent().addClass('unsaved');
  });
  $('.control-input .input-save').click(function () {
    $(this).parent().parent().removeClass('unsaved');
  });
  $('.control-input .input-cancel').click(function () {
    $(this).parent().parent().removeClass('unsaved');
  });

  // Close dat modal
  $('.btn-close-modal').click(function () {
    $(this).parents('.gu-modal').removeClass('slds-fade-in-open');
    $('.slds-backdrop--open').removeClass('slds-backdrop--open');
  });

});

// Close the modal on esc
$(document).on('keyup',function(evt) {
  if (evt.keyCode === 27) {
    $('.gu-modal').removeClass('slds-fade-in-open');
    $('.slds-backdrop--open').removeClass('slds-backdrop--open');
  }
});

// toggle selected search filters
$(document).ready(function() {
  $('.filter-opt').click(function () {
    $(this).toggleClass('active');
  });
});

// Expand hidden table rows
$('.table-default .expand-row').click(function () {
  $(this).next('.detail-row').toggleClass('slds-hide');
});


$(document).ready(function() {
  // Fix the anchor problem created with the <base> tag
  var pathname = window.location.href.split('#')[0];
  $('a[href^="#"]').each(function() {
    var $this = $(this),
      link = $this.attr('href');
    $this.attr('href', pathname + link);
  });
});


setTimeout( function(){
  // Firefox cant handle the flex order property with absolutely positioned elements
  $('#nav-shrink').css('position', 'absolute');
  $('label[for="nav-shrink"]').css('position', 'absolute');
  // Add the spinner and overlay to pretend the page is loading
  //$('.gu-spinner').addClass('fade-in');
}, 10 );

setTimeout( function(){
  // Add a fade class to the spinner after 0.9s
  $('.gu-spinner').addClass('fade-out');
}, 900 );

setTimeout( function(){
  // display:none the spinner after 1s
  $('.gu-spinner').hide();
}, 1000 );

