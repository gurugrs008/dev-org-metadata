trigger AccountAddressTrigger on Account (before insert, before update) {
    For(Account a : Trigger.New){
        if(a.BillingPostalCode <> '' && a.Match_Billing_Address__c == True){
            a.ShippingPostalCode = a.BillingPostalCode;
        }
    }

}