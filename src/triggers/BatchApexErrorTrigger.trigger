trigger BatchApexErrorTrigger on BatchApexErrorEvent (after insert) {
    List<BatchLeadConvertErrors__c> bcleList = new List<BatchLeadConvertErrors__c>();
    for(BatchApexErrorEvent  baee : Trigger.New){
        BatchLeadConvertErrors__c blce = new BatchLeadConvertErrors__c();
        blce.AsyncApexJobId__c = baee.AsyncApexJobId;
        blce.Records__c = baee.JobScope;
        blce.StackTrace__c = baee.StackTrace;
        bcleList.add(blce);
    }
    insert bcleList;
}