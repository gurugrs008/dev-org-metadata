trigger OrderEventTrigger on Order_Event__e (after insert) {
    
    List<Task> taskList = new List<Task>();
    
    for(Order_Event__e event : Trigger.new){
        if(event.Has_Shipped__c == true){
            Task t = new Task();
            t.Priority = 'Medium';
            t.subject = 'Follow up on shipped order '+event.Order_Number__c;
            t.OwnerId = event.CreatedById;
            taskList.add(t);
        }
    }
	insert taskList;
}