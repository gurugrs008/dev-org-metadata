trigger InsertTeacherTrigger on Contact (before insert, before update) {
    if(Trigger.isBefore){
        if(Trigger.isInsert || Trigger.isUpdate){
          for(Contact c: Trigger.New){
        	if(c.subjects__C.contains('Hindi')){
            c.addError('You cant insert this record');
        	}
    	  }        
    	}
    }       
}