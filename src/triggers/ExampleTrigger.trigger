trigger ExampleTrigger on Contact (before insert, after delete) {
    if(Trigger.isInsert){
        Integer count = Trigger.new.size();
        EmailManager.sendMail('gurugrs008@gmail.com', 'Trigger Mail on Contact Insertion', 'A new Contact is inserted into the system');
        System.debug('Number of Contacts inserted: '+count);
    }
    else if(Trigger.isDelete){
        //Will code later
    }
}