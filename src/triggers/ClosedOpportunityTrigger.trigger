trigger ClosedOpportunityTrigger on Opportunity (after insert,after update) {
    List<Task> taskList = new List<Task>();
    List<Opportunity> oppList = [Select Id, Name, StageName, OwnerId from Opportunity where Id In: Trigger.New and StageName = 'Closed Won'];
    if(trigger.isInsert){
        if(oppList.size() > 0){
            for(Opportunity opp : oppList){
                taskList.add(new Task(whatId = opp.Id,subject = 'Follow Up Test Task',ownerId = opp.OwnerId));
            }
        }
    }
    if(trigger.isUpdate){
        if(oppList.size() > 0){
            for(Opportunity opp : oppList){
                if(Trigger.oldMap.get(opp.Id).StageName != 'Closed Won' && opp.StageName == 'Closed Won'){
                	taskList.add(new Task(whatId = opp.Id,subject = 'Follow Up Test Task',ownerId = opp.OwnerId));
                }
             }
        }
    }
    
    if(taskList.size() > 0){
        insert taskList;
    }
}