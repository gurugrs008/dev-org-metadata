trigger ClassTrigger on Class__c (before delete, before Insert, before update) {
    if(Trigger.isDelete){
        if(Trigger.isBefore){
            for(Class__c c: Trigger.Old){
                ClassHandler.beforeDeleteHandler(Trigger.Old);
            }
        }
    }
    if(Trigger.isInsert || Trigger.isUpdate){
        if(Trigger.isBefore){
            for(class__c c: Trigger.new){
                ClassHandler.beforeInsertHandler(Trigger.New);
            }
        }
    }

}