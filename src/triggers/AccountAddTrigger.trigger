trigger AccountAddTrigger on Account (before insert, before update) {
    for(Account acc : Trigger.New){
        if(acc.Match_Billing_Address__c == true && String.isNotBlank(acc.BillingPostalCode)){
            acc.ShippingPostalCode = acc.BillingPostalCode;
        }
    }   
}