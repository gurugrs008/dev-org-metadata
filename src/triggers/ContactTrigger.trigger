trigger ContactTrigger on Contact (before insert,after insert, before update, after update, before delete, after delete) {
    switch on Trigger.operationType{
        when BEFORE_INSERT{
            ContactTriggerController.beforeInsert(Trigger.new);
        }
        when After_INSERT{
            ContactTriggerController.afterInsert(Trigger.new);
        }
        when BEFORE_UPDATE{
            ContactTriggerController.beforeUpdate(Trigger.newMap,Trigger.oldMap);
        }
        when After_UPDATE{
            ContactTriggerController.afterUpdate(Trigger.newMap,Trigger.oldMap);
        }
    }
}