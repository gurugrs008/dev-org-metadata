public with sharing class Stack_1 {

private List<Object> items {get; set;}
   
   public Stack_1() {
       this.items = new List<Object>();
   }
   
   public Integer size() {
       return this.items.size();
   }

   public Boolean isEmpty() {
       return size() == 0;
   }
    public void push(Object lo){
        this.items.add(lo);
    }
    public Object pop(){
        if(isEmpty()){
            //throw new StackUnderFlowException();
        }
        return this.items.remove(size()-1);
    }
    public Object peek(){
        if(isEmpty()){
            //throw new StackUnderFlowException();
           }
        return this.items.get(size()-1);
    }
}