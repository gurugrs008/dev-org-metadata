public class Students_In_Class {
public static List<Student__c> number_of_students(String class_id){
    List<Student__c> nos = new List<Student__c>();
    nos = [Select name from Student__c where class__r.name =: class_id];
    for(Student__c s:nos){
        System.debug('Student Name '+s.name);
    }
    return nos;
}


public static Integer no_of_days(date startDate, date endDate){
  Integer i = 0;
    If (endDate > startDate){
   while (startDate < endDate) {
   	DateTime startDateTime = (DateTime) startDate;
       if (startDateTime.format('E') != 'Sat' && startDateTime.format('E') != 'Sun') {
           i++;
       }
       startDate = startDate.addDays(1);
   }

   return i;
   }

    Else{
        
         System.debug('End Date cannot be greater than Start Date.');
        return 0;
    }
}
}