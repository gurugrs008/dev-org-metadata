public with sharing class ContactController {
    @AuraEnabled
    public static List<Contact> retrieveContactList(Integer cursor){
        System.debug(cursor);
        Integer off = Integer.valueOf(cursor);
        List<Contact> ctctList = [Select id, name, email, phone, Level__c, Profile_Photo_URL__c
                                  FROM Contact 
                                  Order by name 
                                  limit 5
                                  Offset : off];
		return ctctList;        
    }
    @AuraEnabled
    public static List<Contact> retrieveAllContacts(){
        List<Contact> ctctList = [Select id, name, email, phone, Level__c, Profile_Photo_URL__c
                                  FROM Contact 
                                  Order by name ];
		return ctctList;        
    }
}