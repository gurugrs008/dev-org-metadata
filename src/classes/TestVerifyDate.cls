@isTest
private class TestVerifyDate {
    
    private static Date selectedDate =  Date.today(); //Give your date
	private static Date firstDate = selectedDate.toStartOfMonth();
	private static Date lastDate = firstDate.addDays(date.daysInMonth(selectedDate.year() , selectedDate.month())  - 1);
    
    @isTest private static void CheckDatesTest1(){
        Date d1 = VerifyDate.CheckDates(Date.Today(),Date.Today() - 1);
        System.assertEquals(d1, lastDate);
    }
    @isTest private static void CheckDatesTest2(){
        Date d1 = verifyDate.CheckDates(Date.Today(),Date.Today() + 1);
        System.assertEquals(d1, Date.today()+1);
    }
    @isTest private static void CheckDatesTest3(){
        Date d1 = verifyDate.CheckDates(Date.Today(),Date.Today() + 31);
        System.assertEquals(d1, lastDate);
    }

}