public class ContactTriggerController {
	public static Boolean runTrigger = true;
    public static void beforeInsert(List<Contact> newContacts){
        Set<Id> accountIds = new Set<Id>();
        Map<Id,List<Contact>> accToConListMap = new Map<Id,List<Contact>>();
        if(newContacts.size() > 0){
            for(Contact con : newContacts){
                accountIds.add(con.AccountId);
                List<Contact> conList = new List<Contact>();
                if(!accToConListMap.containsKey(con.AccountId)){
                    accToConListMap.put(con.AccountId, new List<Contact>());
                }
                conList = accToConListMap.get(con.AccountId);
                conList.add(con);
                accToConListMap.put(con.AccountId, new List<Contact>());
            }			            
        }
        List<Contact> contList;
        for(Account acct : [Select Id,Name,Email__c from Account where id in: accToConListMap.keyset()]){ 
            for(Contact cont : accToConListMap.get(acct.Id)){
                cont.Email = acct.Email__c;
                contList.add(cont);
            }        
        }
        update contList;
    }
    public static void afterInsert(List<Contact> newContacts){
        
    }
    public static void beforeUpdate(Map<Id,Contact> newMap, Map<Id,Contact> oldMap){
        
    }
    public static void afterUpdate(Map<Id,Contact> newMap,Map<Id,Contact> oldMap){
        //AccountTriggerController.runTrigger = false;
        List<Contact> contList = new List<Contact>();
        Set<Id> accountIdSet = new Set<Id>();
        List<Account> acctList = new List<Account>();
        if(runTrigger){
            if(newMap.values().size() > 0){
                for(Contact cont : newMap.values()){
                    if(newMap.get(cont.Id).Email != null){
                        if(!newMap.get(cont.Id).Email.equalsIgnoreCase(oldMap.get(cont.Id).Email)){
                            contList.add(cont);
                            if(!accountIdSet.contains(cont.AccountId)){
                                accountIdSet.add(cont.AccountId);
                                Account acct = new Account(Id = cont.AccountId);
                                acct.Email__c = cont.Email;
                                acctList.add(acct);                            
                            }
                        }
                    }
                }
                update acctList;
            }            
        }            
    }
}