global class campingListItemApexController {
    @auraEnabled
    global static List<Camping_Item__c> getCampingItems(){
        List<Camping_Item__c> campItemList = [Select Id, Name,Packed__c, Price__c, Quantity__c from Camping_Item__c limit 1];
        return campItemList;
    }

}