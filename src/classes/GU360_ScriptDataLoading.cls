global with sharing class GU360_ScriptDataLoading {
	
	global static String getDemoData(String calloutType) {
		PageReference demoPage;
		if(calloutType.toLowerCase() == 'Contact') {
			demoPage = PageReference.forResource('AccountJSONResource','/AccountJSONResource/ContactJSONData.json');
		}
		return String.valueOf(demoPage.getContent().toString());        
	}
    
}