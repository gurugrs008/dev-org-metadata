public class ForNehasAssignment {
    public static Map<Id,List<Contact>> fetchAccountContacts(){
        Map<Id,List<Contact>> mapAccountToContact = new Map<Id,List<Contact>>();
        for(Contact con : [Select Id, Name, AccountId, Account.Name from Contact limit 100]){
            List<Contact> conList = new List<Contact>();
            conList.add(con);
            if(mapAccountToContact.containsKey(con.AccountId)){
                conList = mapAccountToContact.get(con.AccountId);
                conList.add(con);                
            }
            System.debug(con.AccountId);
            System.debug(con.Name);
            mapAccountToContact.put(con.AccountId, conList);
        }
        return mapAccountToContact;
    }
}