public class AddPrimaryContact implements Queueable{
	/*testing*/
    private String state;
    private Contact ctct;
    public AddPrimaryContact(Contact ct, String state){
        this.ctct = ct;
        this.state = state;
    }
    public void execute(QueueableContext qc){
        List<Account> accountList = [Select id,BillingState from Account where BillingState =: state limit 200];
        List<Contact> contactList = new List<Contact>();
        for(Account acct: accountList){
            Contact cont = ctct.clone(false,false,false,false);
            cont.AccountId = acct.id;
            contactList.add(cont);
        }
        if(contactList.size()>0){
            insert contactList;
        }
        
    }

}