public class ClassHandler{
    public static void beforeDeleteHandler(List<Class__c> classList){
        for(Class__c cls: classList){
            if(cls.Number_of_Femal_Students__c > 1){
                cls.addError('You cant delete this record as it has more than one female student associated');
            }
        }
    }
    public static void beforeInsertHandler(List<class__C> classList){
        For(Class__C cls: classList){
            if(cls.Number_of_Students__c > cls.MaxSize__c){
                cls.addError('You cant insert this record as Max size of class is already reached');
            }
        }
    }
}