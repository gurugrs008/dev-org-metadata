public class AccountProcessor {
    @future
    public static void countContacts(List<Id> idList){
		/*testing*/
        List<Account> acnt = [Select name,number_of_contacts__c from Account where id in: idList];
        List<Account> accountListToUpdate = new List<Account>();
        List<Contact> cntct = [Select id,lastname,accountid from Contact where accountid in: idList];
        //system.assert(false, '===>' + idLIst);
        for(Id i : idList){
            Integer count;
            for(Contact c: cntct){
                count = 0;
                if(c.Accountid == i){
                    count++;
                }
            }
            for(Account acc1 : acnt){
                if(acc1.id == i){
                    acc1.number_of_contacts__c = count;
                    accountListToUpdate.add(acc1);
                }
            }
            
            //Integer n = cntct.size();
            //Schema.Account acc1 = a.getSObjectType();
        }
        
        update accountListToUpdate;
    }

}