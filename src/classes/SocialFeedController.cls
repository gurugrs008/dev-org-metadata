global with sharing class SocialFeedController {

	@AuraEnabled
	global static SocialPost[] retrievePosts(String socialFeedApiName) {

        /*
        List<String> lstFeed  = new List<String>(GU360_Feed__mdt.SObjectType.getDescribe().fields.getMap().keySet());
        List<String> lstFeedSource  = new List<String>(GU360_Feed_Source__mdt.SObjectType.getDescribe().fields.getMap().keySet());
        String strsoql = 'SELECT '+String.join(lstFeed, ', ')+',( SELECT '+String.join(lstFeedSource, ', ') +' FROM GU360_Feed_Sources__r ) FROM GU360_Feed__mdt WHERE DeveloperName =:socialFeedApiName';
		*/
		List<SocialPost> allPosts = new List<SocialPost>();
		//retrieve Feed metadata and related child feed sources		
		System.debug('Inside retrievePosts');
		for (GU360_Feed__mdt feed : [SELECT Id,(SELECT Id,Feed_Provider_API_Name__c,Identifier__c,Post_Limit__c FROM GU360_Feed_Sources__r)FROM GU360_Feed__mdt WHERE DeveloperName =:socialFeedApiName]) {			
			for (GU360_Feed_Source__mdt feedSource : feed.GU360_Feed_Sources__r) {
				Type t = Type.forName(feedSource.Feed_Provider_API_Name__c);
				SocialFeedController.SocialFeedProvider sfp = (SocialFeedController.SocialFeedProvider) t.newInstance();
				List<SocialPost> posts = new List<SocialPost>(sfp.retrievePosts(feedSource.identifier__c,Integer.valueOf(feedSource.Post_Limit__c)));
				allPosts.addAll(posts);
			}
		}

		//Sort all returned sorces together chronologically (most recent first) and return the full list.
		allPosts.sort();
		
		//retrieve Feed metadata
		//retrieve related child feed sources
		//iterate sources, instantiate each as a SocialFeedProvider and run retrievePosts() method passing in identifier field from related source.
		return allPosts;
	}

	@AuraEnabled
	global static List<FeedSource> retrieveSourceOptions(String socialFeedApiName) {
		List<String> lstFeed  = new List<String>(GU360_Feed__mdt.SObjectType.getDescribe().fields.getMap().keySet());
		List<String> lstFeedSource  = new List<String>(GU360_Feed_Source__mdt.SObjectType.getDescribe().fields.getMap().keySet());
		String strsoql = 'SELECT '+String.join(lstFeed, ', ')+',( SELECT '+String.join(lstFeedSource, ', ') +' FROM GU360_Feed_Sources__r ) FROM GU360_Feed__mdt WHERE DeveloperName =:socialFeedApiName';
		List<FeedSource> sources = new List<FeedSource>();
        System.debug('Inside retrieveSourceOptions');
		for(GU360_Feed__mdt feed : Database.query(strsoql)) {
			for(GU360_Feed_Source__mdt feedSource : feed.GU360_Feed_Sources__r) {
				FeedSource source = new FeedSource();
				source.label = feedSource.Label__c;
				source.required = feedSource.Required__c;
				source.defaultOn = feedSource.Default_On__c;
				sources.add(source);
			}
		}
		return sources;
	}

	global class SocialProviderOption {
		@AuraEnabled
		global String label{get;set;}
		@AuraEnabled
		global String defaultOn{get;set;}
		@AuraEnabled
		global String enabled{get;set;}
		@AuraEnabled
		global String required{get;set;}

	}

	global interface SocialFeedProvider {
		List<SocialPost> retrievePosts(String identifier, Integer postLimit);
	}

	global class SocialPost implements Comparable {
		@AuraEnabled
		global String authorName{get;set;}
		@AuraEnabled
		global String authorPhotoUrl{get;set;}
		@AuraEnabled
		global String authorUsername{get;set;}
		@AuraEnabled
		global String authorLink{get;set;}
		@AuraEnabled
		global String content{get;set;}
		@AuraEnabled
		global String feedTypeIconUrl{get;set;}
		@AuraEnabled
		global DateTime postDate{get;set;}
		@AuraEnabled
		global String relativePostDateString{get;set;}		
		@AuraEnabled
		global Boolean hasPhoto{get;set;}
		@AuraEnabled
		global Boolean hasVideo{get;set;}
		@AuraEnabled
		global String mediaUrl{get;set;}
		@AuraEnabled
		global Integer mediaWidth{get;set;}
		@AuraEnabled
		global Integer mediaHeight{get;set;}
        @AuraEnabled 
        global String tweetOrFbUrl{get;set;}
        @AuraEnabled 
        global Integer tweetCount{get;set;}
		
		//Sorting by postDate descending nulls last
		public Integer compareTo(Object obj) {
	        SocialPost sp = (SocialPost)(obj);
	        if(this.postDate == null){
	        	return 1;
	        }
	        if(sp.postDate == null){
	        	return -1;
	        }
	        
		    if (this.postDate > sp.postDate) {
		        return -1;
		    }
		    if (this.postDate == sp.postDate) {
		        return 0;
		    }
		    return 1;
	    }
	}

	global class FeedSource {
		@AuraEnabled
		global String label{get;set;}
		@AuraEnabled
		global Boolean required{get;set;}
		@AuraEnabled
		global Boolean defaultOn{get;set;}

	}

}