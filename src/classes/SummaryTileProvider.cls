global class SummaryTileProvider {
	global class AccountFieldsWrapper extends LADS.RecordBase{
        @AuraEnabled
        global String id{get;set;}
        @AuraEnabled
        global String AccountName{get;set;}        
        @AuraEnabled
        global Double AnnualRevenue{get;set;}
        @AuraEnabled
        global String Description{get;set;}
        @AuraEnabled
        global String Industry{get;set;}
        @AuraEnabled
        global String LastModifiedById{get;set;}        
        @AuraEnabled
        global Double NumberOfContacts{get;set;}
        @AuraEnabled
        global String ParentAccountId{get;set;}               
    }
    @AuraEnabled
    global static AccountFieldsWrapper retrieveRecord() {
        List<String> fieldsList  = new List<String>(Account.SObjectType.getDescribe().fields.getMap().keySet());
        String accountfields = String.join(fieldsList,',');
        // Querying Account_Engagement_Summary__c records to be processed.
        String query = 'Select '+accountfields+ ' FROM Account'
            +' WHERE Id = \'0012800000r69TvAAI\'';        				
        
        //Reusable variable for queried summaries
        List<Account> queriedAccounts = (List<Account>)Database.query(query);
        AccountFieldsWrapper afw;
        //List<AccountFieldsWrapper> afwList = new List<AccountFieldsWrapper>();
        for(Account acc : queriedAccounts){
            afw = new AccountFieldsWrapper();
            afw.id = acc.Id;
            afw.AccountName = acc.Name;
            afw.AnnualRevenue = acc.AnnualRevenue;
            afw.Description = acc.Description;
            afw.Industry = acc.Industry;
            afw.LastModifiedById = acc.LastModifiedById;
            afw.NumberOfContacts = acc.Number_of_Contacts__c;
            afw.ParentAccountId = acc.ParentId;
            //afwList.add(afw);
        }
        return afw;
    }
}