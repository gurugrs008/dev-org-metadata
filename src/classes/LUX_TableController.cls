global with sharing class LUX_TableController {
    @AuraEnabled
    global static List<TableColumn> retrieveTableColumns(String tableApiName) {
        //retrieve columns for table
        
        LUX_Column__mdt[] gu360Columns = [SELECT Id,Label, Link_Destination_API_Name__c,QualifiedApiName, DeveloperName,Link_Label_Api_Name__c,Link_Behavior__c,Is_Resizeable__c,Column_Header__c,Image_Width__c,Image_Height__c,Order__c,Is_SorTable__c,Column_Class__c,Content_API_Name__c,Content_Type__c,Show_Header__c,Sortable_Field_API__c,Sortable_Field_Data_Type__c,Description__c FROM LUX_Column__mdt WHERE LUX_Table_Name__c = :tableApiName ORDER BY Order__c ASC,Id ASC];
            List<TableColumn> columns = new List<TableColumn>();      
            for(LUX_Column__mdt guCol : gu360Columns) {
                TableColumn col = new TableColumn();
                col.order = guCol.Order__c != null ? Integer.valueOf(guCol.Order__c) : 1;
                col.contentType = guCol.Content_Type__c;
                col.linkDestinationApiName = guCol.Link_Destination_API_Name__c;
                col.showHeader = guCol.Show_Header__c;
                col.contentApiName = guCol.Content_API_Name__c;
                col.columnClass = guCol.Column_Class__c;
                col.label = guCol.Label;
                col.isSorTable = guCol.Is_SorTable__c;
                col.isResizeable = guCol.Is_Resizeable__c;
                col.columnHeader = guCol.Column_Header__c;
                col.imageHeight = guCol.Image_Height__c != null ? Integer.valueOf(guCol.Image_Height__c) : 0;
                col.imageWidth = guCol.Image_Width__c != null ? Integer.valueOf(guCol.Image_Width__c) : 0;
                col.linkBehavior = guCol.Link_Behavior__c;
                col.linkLabelApiName = guCol.Link_Label_Api_Name__c;
                col.developerName = guCol.DeveloperName;
                col.sortableFieldAPI = guCol.Sortable_Field_API__c;
                col.sortableFieldDataType = guCol.Sortable_Field_Data_Type__c;
                col.description = guCol.Description__c;
                columns.add(col);
            }
            return columns;

    }

     /*******************************************************************************************************
    * @description Wrapper class for Column.
    * 
    */
    global class TableColumn {
       /*******************************************************************************************************
          * @description get and set order.
          * @return Integer
          */    
        @AuraEnabled
        global Integer order{get;set;}
         /*******************************************************************************************************
          * @description get and set contentType.
          * @return String
          */
        @AuraEnabled
        global String contentType{get;set;}
        /*******************************************************************************************************
          * @description get and set columnHeader.
          * @return String
          */
        @AuraEnabled
        global String columnHeader{get;set;}
        /*******************************************************************************************************
          * @description get and set isResizeable.
          * @return Boolean
          */
        @AuraEnabled
        global Boolean isResizeable{get;set;}        
        /*******************************************************************************************************
          * @description get and set imageWidth.
          * @return Integer
          */
        @AuraEnabled
        global Integer imageWidth{get;set;}        
        /*******************************************************************************************************
          * @description get and set imageHeight.
          * @return Integer
          */
        @AuraEnabled
        global Integer imageHeight{get;set;}                
       /*******************************************************************************************************
          * @description get and set linkBehavior.
          * @return String
          */   
        @AuraEnabled
        global String linkBehavior{get;set;}                        

        @AuraEnabled
        global String linkDestinationApiName{get;set;}

       /*******************************************************************************************************
          * @description get and set linkLabelApiName.
          * @return String
          */ 
        @AuraEnabled
        global String linkLabelApiName{get;set;}
       /*******************************************************************************************************
          * @description get and set showHeader.
          * @return Boolean
          */ 
        @AuraEnabled
        global Boolean showHeader{get;set;}
      /*******************************************************************************************************
          * @description get and set isSorTable.
          * @return Boolean
          */
        @AuraEnabled
        global Boolean isSorTable{get;set;}
         /*******************************************************************************************************
          * @description get and set iconUrl.
          * @return String
          */
        @AuraEnabled
        global String iconUrl{get;set;}
        /*******************************************************************************************************
          * @description get and set contentApiName.
          * @return String
          */
        @AuraEnabled
        global String contentApiName{get;set;}
        /*******************************************************************************************************
          * @description get and set columnClass.
          * @return String
          */   
        @AuraEnabled
        global String columnClass{get;set;}
         /*******************************************************************************************************
          * @description get and set label.
          * @return String
          */
        @AuraEnabled
        global String label{get;set;}
        //added by Gaurav
         /*******************************************************************************************************
          * @description get and set id.
          * @return String
          */
        @AuraEnabled
        global Id id{get;set;}
        /*******************************************************************************************************
          * @description get and set fullName.
          * @return String
          */   
        @AuraEnabled
        global String fullName{get;set;} 
        /*******************************************************************************************************
          * @description get and set DeveloperName.
          * @return String
          */   
        @AuraEnabled
        global String developerName{get;set;}
        /*******************************************************************************************************
          * @description get and set sortableFieldAPI.
          * @return String
          */   
        @AuraEnabled
        global String sortableFieldAPI{get;set;} 
        /*******************************************************************************************************
          * @description get and set sortableFieldDataType.
          * @return String
          */   
        @AuraEnabled
        global String sortableFieldDataType{get;set;}
        /*******************************************************************************************************
          * @description get and set description for columns.
          * @return String
          */   
        @AuraEnabled
        global String description{get;set;}
        
        
    }   
}