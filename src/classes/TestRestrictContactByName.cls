@isTest
private class TestRestrictContactByName {
    private static Contact con;
    @isTest private static void TestContactBeforeInsertPositive(){
        con = new Contact();
        con.lastname = 'Gyamlani';
        insert con;
    }
    
    @isTest private static void TestContactBeforeInsertNegative(){
        con = new Contact();
        con.lastname = 'INVALIDNAME';
        insert con;
    }
    
    @isTest private static void TestContactBeforeUpdate(){
        List<Contact> cntctList = [Select lastname from Contact where lastName = 'Goyal'];
        List<Contact> cntctsToUpdate = new List<Contact>();
        //con = new Contact();
        for(Contact ctct: cntctList){
            con.lastname = 'INVALIDNAME';
            cntctsToUpdate.add(con);
        }
        
        update cntctsToUpdate;
    }
}