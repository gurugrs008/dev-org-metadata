//Lightning Apex Data Service
/**
*@author Erik Golden
*@date May 2017
*@description //Lightning Apex Data Service Global class which provides interface for saving and retrieving records and recordsets
*@example 
*/
global with sharing class LADS {

    /**
    *@author Erik Golden
    *@date Oct 2017
    *@description Global wrapper class for batch LADS data responses 
    */       
    global class BatchResponse {
        @AuraEnabled
        global Map<String,LADS.RecordBase> records{get;set;}
        @AuraEnabled
        global Map<String,List<LADS.RecordBase>> recordSets{get;set;}   
    }

	/**
    *@author Erik Golden
    *@date May 2017
    *@description Global wrapper class for specifying attributes for filtering data
    *@example FilterCriteria will specify propert, operator, value and type 
	*/    
    global class FilterCriteria {
        @AuraEnabled
        global String property{get;set;} 
        @AuraEnabled
        global String operator{get;set;}  //equals,notEquals,lessThan,lessThanEqualTo,greaterThan,greaterThanEqualTo,contains (string only)
        @AuraEnabled
        global String value{get;set;} //String representation of value
        @AuraEnabled
        global String valueType{get;set;} //String,Integer,Date,DateTime,Decimal,Double,Long,etc. valid primitive type
    }
    
	/**
    *@author Erik Golden
    *@date May 2017
    *@description Global abstract class for specifying id of record
    *@example RecordBase is used as a return type for save and retrieve methods
	*/   
    global abstract class RecordBase {
        @AuraEnabled
        global String id{get;set;}       
    }
    
	/**
    *@author Erik Golden
    *@date May 2017
    *@description Global interface for retrieving and saving Record
    *@example global class FacultyBioProvider implements LADS.RecordDataProvider
	*/   
    global interface RecordDataProvider { 
        RecordBase retrieveRecord(String url);
    }
    
	/**
    *@author Erik Golden
    *@date May 2017
    *@description Global interface for retrieving and saving Editable Record
    *@example EditableRecordDataProvider is a base interface that extends RecordDataProvider
	*/      
    global interface EditableRecordDataProvider extends RecordDataProvider { 
        RecordBase saveRecord(String url, String data);  //Lightning currently doesn't fully support passing object parameters to methods  
    }
    
	/**
    *@author Erik Golden
    *@date May 2017
    *@description Global interface for retrieving  Editable Record Sets
    *@example global class FacultyEducationProvider implements LADS.RecordSetDataProvider
	*/        
    global interface RecordSetDataProvider {
        RecordBase[] retrieveRecordSet(String url); //actually the current url
    }
    
	/**
    *@author Erik Golden
    *@date May 2017
    *@description Global interface for deleting, retrieving and saving Editable Record Sets
    *@example EditableRecordSetDataProvider methods return instance of RecordBase and extends RecordSetDataProvider
	*/         
    global interface EditableRecordSetDataProvider extends RecordSetDataProvider {
        RecordBase saveRecordSetItem(String url, String item); //new data and current URL
        RecordBase deleteRecordSetItem(String url, String item); //old data and current URL
    }
    
	/**
    *@author Erik Golden
    *@date May 2017
    *@description Global interface for deleting, retrieving filtered record set and extends LADS.RecordSetDataProvider
    *@example FilterableRecordSetDataProvider method returns instance of RecordBase List and extends RecordSetDataProvider
	*/     
    global interface FilterableRecordSetDataProvider extends LADS.RecordSetDataProvider {
        RecordBase[] retrieveFilteredRecordSet(String url, LADS.FilterCriteria[] filterCriteria,Integer resultLimit,Integer resultOffset);
    }


}