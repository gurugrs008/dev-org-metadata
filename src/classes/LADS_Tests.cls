/**
* @author Sandeep Yadav @ Appirio
* @date 2017
*
* @description This is a test class of LADS_Controller and of LADS_Controller
* @className LADS_Tests 
*/
@isTest
public class LADS_Tests {
    
    /*******************************************************************************************************
@description This is a test of retrieveRecord method LADS_Controller
*/
    @isTest
    static  void testRetrieveRecord() {
        try {
            String urlStr = 'https://www.testurl.com/faculty/s?id=';
            String className = 'LADS_SampleController.RecordCtrl';
            LADS.RecordBase baseData = LADS_Controller.retrieveRecord(className ,urlStr);
            LADS_SampleController.Sample data = (LADS_SampleController.Sample)baseData;
            system.assert(data.name == 'THIS IS A TEST');
            LADS.RecordBase baseData2 = LADS_Controller.retrieveRecord('' ,'');
            system.assert(baseData2 == null);
        } catch(Exception e) {
            System.debug(e);
        }
    }
    
    
        /*******************************************************************************************************
    @description This is a test of saveRecord method of LADS_Controller
    */
    
    @isTest
    static  void testSaveRecord() {
        try {
            String urlStr = 'https://www.testurl.com/faculty/s?id=';
            String className = 'LADS_SampleController.RecordCtrl';
            LADS.RecordBase baseData = LADS_Controller.retrieveRecord(className ,urlStr);
            LADS_SampleController.Sample data = (LADS_SampleController.Sample)baseData;
            LADS.RecordBase baseData2 = LADS_Controller.saveRecord(className ,urlStr,JSON.serialize(data));
            LADS.RecordBase baseData3 = LADS_Controller.saveRecord('' ,'','');
            system.assert(baseData2 == null);
        } catch(Exception e) {
            System.debug(e);
        }       
    }
    
    /*******************************************************************************************************
    @description This is a test of retrieveRecordSet method of LADS_Controller
    */
    @isTest
    static  void testRetrieveRecordSet() {
        try {
            String urlStr = 'https://www.testurl.com/faculty/s?id=';
            String className = 'LADS_SampleController.RecordSetCtrl';
            LADS.RecordBase[] baseData = LADS_Controller.retrieveRecordSet(className ,urlStr);
            List<LADS_SampleController.Sample> data = (List<LADS_SampleController.Sample>)baseData;            
            LADS.RecordBase[] baseData2 = LADS_Controller.retrieveRecordSet('' ,'');
            system.assert(baseData2.size() == 0);
        } catch(Exception e) {
            System.debug(e);
        }      
    }
    
    /*******************************************************************************************************
    @description This is a test of Editable retrieveRecordSet method of LADS_Controller
    */
    @isTest
    static  void testEditableRetrieveRecordSet() {
        try {
            String urlStr = 'https://www.testurl.com/faculty/s?id=';
            String className = 'LADS_SampleController.EditableRecordSetCtrl';
            LADS.RecordBase[] baseData = LADS_Controller.retrieveRecordSet(className ,urlStr);
            List<LADS_SampleController.Sample> data = (List<LADS_SampleController.Sample>)baseData; 
            System.assert(data.size()>0);
            //LADS.RecordBase[] baseData2 = LADS_Controller.retrieveRecordSet('','');
        } catch(Exception e) {
            System.debug(e);
        }      
    }

    /*******************************************************************************************************
@description This is a test of retrieveFilteredRecordSet method of LADS_Controller
*/
    @isTest
    static  void testRetrieveFilteredRecordSet() {
        try {
            String urlStr = 'https://www.testurl.com/faculty/s?id=';
            String className = 'LADS_SampleController.RecordSetCtrl';

            integer resultOffset = 1;
            integer resultLimit = 2;

            LADS.FilterCriteria[] fc = new List<LADS.FilterCriteria>();
            LADS.FilterCriteria l = new LADS.FilterCriteria();
            l.operator = '=';
            l.property = 'name';
            l.value = 'Test';
            l.valueType = 'String';
            
            fc.add(l);
            LADS.RecordBase[] baseData = LADS_Controller.retrieveFilteredRecordSet(className ,urlStr,JSON.serialize(fc),resultOffset,resultLimit);
            List<LADS_SampleController.Sample> data = (List<LADS_SampleController.Sample>)baseData;    
            System.assert(data.size()>0);
            LADS.RecordBase[] baseData2 = LADS_Controller.retrieveFilteredRecordSet('' ,'','',resultOffset,resultLimit);
            
        } catch(Exception e) {
            System.debug(e);
        }     
    }
    

  /*******************************************************************************************************
    @description This is a test of saveRecordSetItem method of LADS.EditableRecordSetDataProvider
    */
    @isTest
    static  void testSaveRecordSetItem() {
        try {
            String urlStr = 'https://www.testurl.com/faculty/s?id=';
            String className = 'LADS_SampleController.EditableRecordSetCtrl';
            LADS_SampleController.Sample samp = new LADS_SampleController.Sample();            
            LADS.RecordBase baseData = LADS_Controller.saveRecordSetItem(className ,urlStr,JSON.serialize(samp));
            LADS.RecordBase baseData2 = LADS_Controller.saveRecordSetItem('' ,'',JSON.serialize(samp));
            system.assert(baseData2 == null);
        } catch(Exception e) {
            System.debug(e);
        }     
    }

  /*******************************************************************************************************
    @description This is a test of saveRecordSetItem method of LADS.EditableRecordSetDataProvider
    */
    @isTest
    static  void testDeleteRecordSetItem() {
        try {
            String urlStr = 'https://www.testurl.com/faculty/s?id=';
            String className = 'LADS_SampleController.EditableRecordSetCtrl';
            LADS_SampleController.Sample samp = new LADS_SampleController.Sample();            
            LADS.RecordBase baseData = LADS_Controller.deleteRecordSetItem(className ,urlStr,JSON.serialize(samp));
            LADS.RecordBase baseData2 = LADS_Controller.deleteRecordSetItem('' ,'',JSON.serialize(samp));
            system.assert(baseData2 == null);
        } catch(Exception e) {
            System.debug(e);
        }     
    }


  /*******************************************************************************************************
    @description This is a test of bulk logic method of LADS Controller
    */
    @isTest
    static  void testBulkLADS() {
        try {
            String urlStr = 'https://www.testurl.com/faculty/s?id=';
            String recordClassName = 'LADS_SampleController.RecordCtrl';
            String recordSetClassName = 'LADS_SampleController.RecordSetCtrl';
            List<String> recordControllers = new List<String>();
            recordControllers.add(recordClassName);
            List<String> recordSetControllers = new List<String>();
            recordSetControllers.add(recordSetClassName);                        
            LADS.BatchResponse br = LADS_Controller.retrieveLADSBulk(recordControllers, recordSetControllers, urlStr);
            system.assert(br.records.containsKey('LADS_SampleController.RecordCtrl'));
            LADS.BatchResponse br2 = LADS_Controller.retrieveLADSBulk(null, null, '');
            system.assert(br2.records.keySet().size() == 0);
        } catch(Exception e) {
            System.debug(e);
        }     
    }
    
}