global with sharing class GU360_Wrappers {
    global class FBPageOverview {
        public String name{get;set;}
        public String link{get;set;}
        public String username{get;set;}
        public String id{get;set;}
        public FBPictureWrapper picture{get;set;}
    }
    
    global class FBPostWrapper {
        public FBPost[] data{get;set;}
        public FBPaging paging{get;set;}
    }
    
    global class FBPostFrom {
        public String name{get;set;}
        public String id{get;set;}
    }
    
    global class FBPictureWrapper {
        public FBPicture data{get;set;}
    }
    
    global class FBPicture {
        public Boolean is_silhouette{get;set;}
        public String url{get;set;}
    }
    
    global class FBPost {
        public String full_picture{get;set;}
        public String created_time{get;set;}
        public String picture{get;set;}
        public String message{get;set;}
        public String link{get;set;}
        public String name{get;set;}
        public String permalink_url{get;set;}
        public String id{get;set;}
        public String type{get;set;}
        public FBPostFrom from_x{get;set;}
    }
    
    class FBPostTag {
        
    }
    
    global class FBPaging {
        public String previous{get;set;}
        public String next{get;set;}
    }
    
    global class FacebookBearerResponse{
        public String access_token{get;set;}
        public String token_type{get;set;}
    }
        
}