global with sharing class GU360_SFDCContextProvider {
    global String recordId{get;set;}
    
    global GU360_SFDCContextProvider(String url) {
        this.recordId = '';
        if(String.isNotBlank(url)){
            if(url.contains('/r/')){
                String[] splitRecordDetailPageURL = url.split('/r/');
                if(splitRecordDetailPageURL.size() > 1){
                    String pageName = splitRecordDetailPageURL[1];
                    String[] splitPageName = pageName.split('/');
                    this.recordId = splitPageName[1];
                } 
                
            }
            else if(url.contains('c__accountId=')) {
                Integer start = url.indexOf('c__accountId=')+13;
                Integer stop;
                String[] splitRecordDetailPageURLById = url.split('c__accountId=');
                if(splitRecordDetailPageURLById.size() > 1){
                    String params = splitRecordDetailPageURLById[1];
                    if(params.contains('&')){
                        if(url.contains('c__')){// this check is for components navigated via passing pageRefernce in URL
                            stop = start + params.indexOf('&');
                        }else{
                            stop = url.indexOf('&');
                        }
                        String recordId = url.substring(start,stop);
                        this.recordId = recordId;
                    }else{
                        this.recordId = splitRecordDetailPageURLById[1];
                    }
                }
            }
            else if(url.contains('c__accountid=')) {
                Integer start = url.indexOf('c__accountid=')+13;
                Integer stop;
                String[] splitRecordDetailPageURLById = url.split('c__accountid=');
                if(splitRecordDetailPageURLById.size() > 1){
                    String params = splitRecordDetailPageURLById[1];
                    if(params.contains('&')){
                        if(url.contains('c__')){// this check is for components navigated via passing pageRefernce in URL
                            stop = start + params.indexOf('&');
                        }else{
                            stop = url.indexOf('&');
                        }
                        String recordId = url.substring(start,stop);
                        this.recordId = recordId;
                    }else{
                        this.recordId = splitRecordDetailPageURLById[1];
                    }
                }
            }
            else if(url.contains('recordUri=')){
                Integer start = url.indexOf('recordUri=')+13;
                Integer stop;
                String[] splitRecordDetailPageURLById = url.split('recordUri=');
                if(splitRecordDetailPageURLById.size() > 1){
                    String params = splitRecordDetailPageURLById[1];
                    if(params.contains('&')){
                        if(url.contains('c__')){// this check is for components navigated via passing pageRefernce in URL
                            stop = start + params.indexOf('&');
                        }else{
                            stop = url.indexOf('&');
                        }
                        String recordId = url.substring(start,stop);
                        this.recordId = recordId;
                    }else{
                        this.recordId = splitRecordDetailPageURLById[1];
                    }
                }
            }
        }
    }
}