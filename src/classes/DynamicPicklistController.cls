public with sharing class DynamicPicklistController {
	@AuraEnabled
    public static List<Contact> retrieveAllContacts(String accName){
        Id accId = [Select id from Account where Name =: accName].id;
        List<Contact> ctctList = [Select id, name, email, phone, Level__c
                                  FROM Contact 
                                  WHERE AccountId =: accId
                                  Order by name ];
		return ctctList;        
    }
}