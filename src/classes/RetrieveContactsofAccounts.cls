public class RetrieveContactsofAccounts {
    public Account account{get;set;}    
    
    public RetrieveContactsofAccounts(ApexPages.standardController std){
        account = (Account)std.getRecord();
    }
    public pagereference save(){
        System.debug('===> Save Method Overrided Invoked');
        if(Account.name =='Test'){
 			return null;
        }
    	upsert account;    
        
        return new pagereference('/'+account.id);
    }
    
    public void Increment(){
        
    }
}