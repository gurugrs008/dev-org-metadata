global class DailyLeadProcessor implements Schedulable{
    
    global void execute(SchedulableContext sc){
        List<Lead> leadList = [Select id,LeadSource from Lead where LeadSource =: '' limit 200];
    	List<Lead> leadToUpdate = new List<Lead>();
        for(Lead ld:leadList){
            ld.LeadSource = 'Dreamforce';
            leadToUpdate.add(ld);
        }
        update leadToUpdate;
    }

}