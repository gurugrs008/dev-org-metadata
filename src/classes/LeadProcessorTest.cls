@isTest
private class LeadProcessorTest {
    @isTest private static void insertLeads(){
        List<Lead> leadList = new List<Lead>();
        for(Integer i=1; i<=200;i++){
            leadList.add(new Lead(Company='Appirio', lastName = 'TestLead'+i, Status='Open - Not Contacted'));
        }
        insert leadList;
        
        Test.startTest();
        LeadProcessor ldp = new LeadProcessor();
        Id batchId = Database.executeBatch(ldp);
        Test.stopTest();
        
    }

}