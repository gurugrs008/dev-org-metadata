public with sharing class MyObjController {    
    @AuraEnabled
    public static List<MyObj__c> getMyObjectsFromApex() {        
        return [SELECT Id,
                       Name,
                       myField__c 
                       FROM MyObj__c];
    }
}