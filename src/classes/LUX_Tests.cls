/**
* @author Appirio Development
* @date 2018
*
* @description This is a test class of LUX_InputPickListController and LUX_TableController
* @className LUX_Tests 

*/
@isTest
private class LUX_Tests {

    @isTest
    static void testLUX_InputPickListController(){
        test.startTest();
        List<String> option = LUX_InputPicklistController.retrievePicklistFieldOptions('Account','Industry');
        System.assert(option.size()>0);
        test.stopTest();
    }
    
    @isTest
    static void testLUX_TableController(){
        test.startTest();
        LUX_TableController.TableColumn tc = new LUX_TableController.TableColumn();
        tc.iconUrl='test';
        tc.fullName='test';
        tc.id=null;
            
        List<LUX_TableController.TableColumn> columns = new List<LUX_TableController.TableColumn>();
        columns  = LUX_TableController.retrieveTableColumns('Faculty_Community_Service_GU_Table');
        System.assert(columns[0].developerName == 'Faculty_Community_Searvice_Involvement');
        test.stopTest();
    }
}