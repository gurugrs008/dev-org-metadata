public class LoadJSONData {
    public static void updateContactRecords(){
        String contactJSON = GU360_ScriptDataLoading.getDemoData('Contact');
        List<Contact> conUpsertList = new List<Contact>();
        List<Contact> conList = (List<Contact>)JSON.deserialize(contactJSON, Contact[].class);
        for(Contact con : conList){
            if(String.isBlank(con.Email)){
                con.Email = 'test@test.com';
                conUpsertList.add(con);
            }
        }
        upsert conUpsertList;
    }
}