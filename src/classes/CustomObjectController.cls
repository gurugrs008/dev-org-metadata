public class CustomObjectController {
	@AuraEnabled
    public static List<Contact> retrieveContactList(){        
        List<Contact> ctctList = [Select id, name, email, phone, Level__c, Profile_Photo_URL__c
                                  FROM Contact 
                                  Order by name 
                                  limit 7];
		return ctctList;        
    }
}