/**
* @author Erik Golden @ Appirio
* @date 2017
*
* @group Lightning Apex Data Service 
* @group-content ../../ApexDocContent/LightningApexDataService.htm
*
* @description Controller that all custom components utilizing the Lightning Apex Data Service
* must either use or extend.  It provides dynamic instantiation of a Data Provider 
* class that implements the LADS.RecordSetDataProvider interface.
*/
global with sharing class LADS_Controller {

	private static final Integer DEFAULT_RESULT_LIMIT = 20;
	

    @AuraEnabled
    global static LADS.BatchResponse retrieveLADSBulk(List<String> recordProviderAPINames,List<String> recordSetProviderAPINames,String url) {
        LADS.BatchResponse br = new LADS.BatchResponse();
        br.records = new Map<String,LADS.RecordBase>();
        br.recordSets = new Map<String,List<LADS.RecordBase>>();
        
        for(String className : recordProviderAPINames) {
            if(!br.records.containsKey(className)) {
                br.records.put(className,LADS_Controller.retrieveRecord(className,url));
            }
        }
        for(String className : recordSetProviderAPINames) {
            if(!br.recordSets.containsKey(className)) {
                br.recordSets.put(className,LADS_Controller.retrieveRecordSet(className,url));
            }
        }
        return br;
    }   
    
	/*******************************************************************************************************
    * @description used by Lightning components to retrieve a record using the Lightning Apex Data Service.
    * @param className the name of a class that implements the LADS.RecordDataProvider interface.
    * @param url the current client url at the time of the request
    * @return the retrieved data of the return type specified by the provider class.  This class will 
    * always extend LADS.BaseRecord.
    * @example retrieveRecordData('ExampleController.SampleProvider','https://...?id=abc123...');
    */
	@AuraEnabled    
    global static LADS.RecordBase retrieveRecord(String className,String url) {
        LADS.RecordBase data;     
        try {
            Type t = Type.forName(className);
            LADS.RecordDataProvider rdp = (LADS.RecordDataProvider)t.newInstance();
            data = rdp.retrieveRecord(url);
        } catch(Exception e) {     	
        	throw new AuraHandledException(e.getMessage()); 	            
        }
        return data;

    }    

    /*******************************************************************************************************
    * @description used by Lightning components to update a record using the Lightning Apex Data Service.
    * @param className the name of a class that implements the LADS.EditableRecordDataProvider interface.
    * @param url the current client url at the time of the request
    * @param data the updated Object data.  Should contain an id property to allow for updates.
    * @return the updated data object.  These fields may differ than the those of the passed in object.
    * @example updateRecordData('ExampleController.SampleProvider','https://...?id=abc123...',data);
    */
	@AuraEnabled    
    global static LADS.RecordBase saveRecord(String className,String url,String data) {
        LADS.RecordBase returnData;
        try {
            Type t = Type.forName(className);
            LADS.EditableRecordDataProvider rdp = (LADS.EditableRecordDataProvider)t.newInstance();
            returnData = rdp.saveRecord(url,data);
        } catch(Exception e) {
        	throw new AuraHandledException(e.getMessage()); 	            
        }
        return returnData;

    }




@AuraEnabled
    global static LADS.RecordBase[] retrieveRecordSet(String className,String url) {
        LADS.RecordBase[] data;	       
        try {
            Type t = Type.forName(className);
            try {
                LADS.EditableRecordSetDataProvider edp = (LADS.EditableRecordSetDataProvider)t.newInstance();
                data = edp.retrieveRecordSet(url);                    
            } catch(Exception ex) {
                LADS.RecordSetDataProvider rdp = (LADS.RecordSetDataProvider)t.newInstance();
                data = rdp.retrieveRecordSet(url);                
            }
            
        } catch(Exception e) {     	
            throw new AuraHandledException(e.getMessage()); 	            
        }
        return data;
        
    }    
    
    
    @AuraEnabled    
    global static LADS.RecordBase[] retrieveFilteredRecordSet(String className,String url,String filterCriteria,Integer resultOffset, Integer resultLimit) {
        LADS.RecordBase[] returnData;
        try {
            
            List<LADS.FilterCriteria> lstFilters = new List<LADS.FilterCriteria>();
            
            if(String.isNotBlank(filterCriteria)) {
                lstFilters = (List<LADS.FilterCriteria>)JSON.deserialize(filterCriteria,List<LADS.FilterCriteria>.class);
            }
            
            resultOffset = (resultOffset != null ? resultOffset : 0);
            resultLimit = (resultLimit != null ? resultLimit : DEFAULT_RESULT_LIMIT);
            Type t = Type.forName(className);
            LADS.FilterableRecordSetDataProvider frsdp = (LADS.FilterableRecordSetDataProvider)t.newInstance();
            returnData = frsdp.retrieveFilteredRecordSet(url,lstFilters,resultOffset,resultLimit);
        } catch(Exception e) {     	
            throw new AuraHandledException(e.getMessage()); 	            
        }
        return returnData;
        
    }  
    
    
    @AuraEnabled    
    global static LADS.RecordBase saveRecordSetItem(String className,String url,String data) {
        LADS.RecordBase returnData;
        try {
            Type t = Type.forName(className);
            LADS.EditableRecordSetDataProvider ersdp = (LADS.EditableRecordSetDataProvider)t.newInstance();
            returnData = ersdp.saveRecordSetItem(url,data);
        } catch(Exception e) {     	
            throw new AuraHandledException(e.getMessage()); 	            
        }
        return returnData;
        
    } 
    
    @AuraEnabled    
    global static LADS.RecordBase deleteRecordSetItem(String className,String url,String data) {
        LADS.RecordBase returnData;
        try {
            Type t = Type.forName(className);
            LADS.EditableRecordSetDataProvider ersdp = (LADS.EditableRecordSetDataProvider)t.newInstance();
            returnData = ersdp.deleteRecordSetItem(url,data);
        } catch(Exception e) {     	
            throw new AuraHandledException(e.getMessage()); 	            
        }
        return returnData;
        
    }    
   
}