public class OlderAccountsUtility {
    public static void updateOlderAccounts(){
        Account[] oldAccounts = [Select id, description from Account order by createdDate asc limit 5];
        for(Account acct : oldAccounts){
            acct.description = 'Heritage Account';
        }
        update oldAccounts;
    }

}