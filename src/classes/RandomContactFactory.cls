public class RandomContactFactory {
    public static List<Contact> generateRandomContacts(Integer n, String lName){
        List<Contact> contactList = new List<Contact>();
        List<Account> accList = [Select id,name from Account where name like 'Test%'];
        for(Integer i=1; i<=n; i++){
            for(Account a: accList){
                if(a.name == 'Test'+i){
                    Contact con = new Contact(firstname = 'Test'+i, lastname = lname, Accountid = a.id);
            		contactList.add(con);
                }
            }
            
        }
        return contactList;
    }
    
    public static List<Account> generateTestAccounts(Integer n){
        List<Account> accList = new List<Account>();
        for(Integer i = 1; i<=n; i++){
            Account acc = new Account(name = 'Test'+i);
            accList.add(acc);
        }
        Return accList;
    }

}