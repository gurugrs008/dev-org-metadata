global with sharing class LUX_InputPicklistController {
    
    @AuraEnabled
    global static List<String> retrievePicklistFieldOptions(String objectName, String fieldName) {
    	
    	List<String> options = new List<String>();
    	Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectName);//From the Object Api name retrieving the SObject
		Sobject Object_name = targetType.newSObject();
		Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
		Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
		Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
		List<Schema.PicklistEntry> pick_list_values = field_map.get(fieldName).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
		for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
			options.add(a.getValue());//add the value  to our final list
		}
		return options;
    }
    
    //Need to define interface for picklist option provider
    /*@AuraEnabled
    global static Object getOptionsData(String className) {
        Type t = Type.forName(className);
        LUX.TypeaheadOptionProvider opIntfc = (LUX.TypeaheadOptionProvider)t.newInstance();
        return opIntfc.getOptions();
    }*/

}