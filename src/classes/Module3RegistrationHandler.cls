/**
  Registration Handler for External Identity Trail
**/
global class Module3RegistrationHandler implements Auth.RegistrationHandler{
    
    private static final String ORG_SUFFIX = '.sso.badge.org';
    private static final String DEFAULT_ACCOUNTNAME = 'Partners';
    private static final String EXTERNAL_USER_PROFILE = 'Partners';
    private static final String INTERNAL_USER_PROFILE = 'Standard User';  
    private static final String TZSID = [SELECT timezonesidkey from User where profile.name = 'System Administrator' LIMIT 1].timezonesidkey;
    
/**
 * Let anyone register as long as the required fields are supplied
 * 
 * We require email, lastName, firstName
 * 
 * @data - the user's info from the Auth Provider
 **/ 
global boolean canCreateUser(Auth.UserData data) {
    System.debug('canCreateUser was called for ' + (data != null ? data.email : 'null'));
    Boolean retVal = (data != null 
            && data.email != null
            && data.lastName != null
            && data.firstName != null);
    
    System.debug('data.username='+data.username);
    System.debug('data.email='+data.email);
    System.debug('data.lastName='+data.lastName);
    System.debug('data.firstName='+data.firstName);
    
    return retVal;
}

/**
 * Create the User - A required method to implement the Handler Interface
 * 
 * @param portalId  - Id of the Community
 * @param data - Auth Provider user data describing the User to create
 * 
 * @return User that has been initialized
**/ 
global User createUser(Id portalId, Auth.UserData data){
    //The user is authorized, so create their Salesforce user
    String googleUser = data.email;

    User u = [SELECT Id, username, email, lastName, firstName, alias, languagelocalekey,
              localesidkey, emailEncodingKey, timeZoneSidKey, profileId
              FROM User WHERE username = :googleUser];

    return u;
}
    
/* old method
global User createUser(Id portalId, Auth.UserData data){
if(!canCreateUser(data)) {
//Returning null or throwing an exception fails the SSO flow
return null;
}
if(data.attributeMap.containsKey('sfdc_networkid')) {
//We have a community id, so create a user with community access
//TODO: Get an actual account
Account a = [SELECT Id FROM account WHERE name='Acme'];
Contact c = new Contact();
c.accountId = a.Id;
c.email = data.email;
c.firstName = data.firstName;
c.lastName = data.lastName;
insert(c);

//TODO: Customize the username and profile. Also check that the username doesn't already exist and
//possibly ensure there are enough org licenses to create a user. Must be 80 characters or less.
User u = new User();
Profile p = [SELECT Id FROM profile WHERE name='Customer Portal User'];
u.username = data.username + '@acmecorp.com';
u.email = data.email;
u.lastName = data.lastName;
u.firstName = data.firstName;
String alias = data.username;
//Alias must be 8 characters or less
if(alias.length() > 8) {
alias = alias.substring(0, 8);
}
u.alias = alias;
u.languagelocalekey = UserInfo.getLocale();
u.localesidkey = UserInfo.getLocale();
u.emailEncodingKey = 'UTF-8';
u.timeZoneSidKey = 'America/Los_Angeles';
u.profileId = p.Id;
u.contactId = c.Id;
return u;
} else {
//This is not a community, so create a regular standard user
User u = new User();
Profile p = [SELECT Id FROM profile WHERE name='Standard User'];
//TODO: Customize the username. Also check that the username doesn't already exist and
//possibly ensure there are enough org licenses to create a user. Must be 80 characters
//or less.
u.username = data.username + '@myorg.com';
u.email = data.email;
u.lastName = data.lastName;
u.firstName = data.firstName;
String alias = data.username;
//Alias must be 8 characters or less
if(alias.length() > 8) {
alias = alias.substring(0, 8);
}
u.alias = alias;
u.languagelocalekey = UserInfo.getLocale();
u.localesidkey = UserInfo.getLocale();
u.emailEncodingKey = 'UTF-8';
u.timeZoneSidKey = 'America/Los_Angeles';
u.profileId = p.Id;
return u;
}
}
*/
global void updateUser(Id userId, Id portalId, Auth.UserData data){
User u = new User(id=userId);
//TODO: Customize the username. Must be 80 characters or less.
//u.username = data.username + '@myorg.com';
//u.email = data.email;
//u.lastName = data.lastName;
//u.firstName = data.firstName;
//String alias = data.username;
//Alias must be 8 characters or less
//if(alias.length() > 8) {
//alias = alias.substring(0, 8);
//}
//u.alias = alias;
update(u);
}
}