public class AwesomeProductController {  
    @AuraEnabled
    public static List<Article__c> getProducts() {
        return [select id, name, photo__c, description__c, points__c from article__c];
    }

    @AuraEnabled
    public static Article__c getProductByName(String name) {
        return [select id, name, photo__c, color__c,
                points__c, description__c,
                (select name from article_size__r order by name)
                from Article__c where name = :name];
    }
}