@isTest
public class DailyLeadProcessorTest {
    //public static String CRON_EXP = '0 0 0 15 3 ? 2022';

   // @testSetup
    
   /* static void setup(){
        List<Lead> leadList=new List<Lead>();
        for(Integer i=1;i<=200;i++){
            leadList.add(new Lead(Company='Appirio', lastName = 'LeadTest'+i, Status='Open - Not Contacted'));
        }
        insert leadList;
    }
    static void testLead(){
        DailyLeadProcessor dlp = new DailyLeadProcessor();
        Test.startTest();
        String jobId=System.schedule('Schedule Daily Lead Processor',CRON_EXP, dlp);
        Test.stopTest();
    }*/
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    @isTest private static void testDailyLeadProcessor(){
        
        List<Lead> leadList=new List<Lead>();
        for(Integer i=1;i<=200;i++){
            leadList.add(new Lead(Company='Appirio', lastName = 'LeadTest'+i, Status='Open - Not Contacted'));
        }
        insert leadList;
        DailyLeadProcessor dlp = new DailyLeadProcessor();
        Test.startTest();
        String jobId=System.schedule('Schedule Daily Lead Processor',CRON_EXP, dlp);
        Test.stopTest();
        
    }
}