public class Account_Contact_Test {
    
public static void asgn_cntct_to_accnts(Id Cntct_id_1){      
    Id actid = [select AccountId from Contact where id =: cntct_id_1].AccountId;
   
    //Query Account record
    //Check if ContactIds for null or not
    
    String ctctids = [Select Contactids__C from Account where id =: actid].Contactids__C;
    If (ctctids == Null)
    {
    	List<Contact> con = [Select id, name from Contact where accountid =: actid];
     
    	String conIds = '';
    	for(contact c : con){
        	conIds += c.Id + ',';        
        	//System.debug('Contact ids are '+ c.id);
    	}
    	conIds = conIds.removeEnd(',');    
    	Account accToUpdate = new Account(Id = actid, contactids__c = conIds);
    	update accToUpdate;
    } 
    else
    {
        if(ctctids.contains(Cntct_id_1))
        {
            
        }
        else
        {
            ctctids += ','+Cntct_id_1;
            Account accToUpdate = new Account(Id = actid, contactids__c = ctctids);
    		update accToUpdate;
        }
    }
  } 
}