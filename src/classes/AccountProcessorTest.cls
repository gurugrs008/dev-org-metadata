@isTest
public class AccountProcessorTest {
    
    @isTest public static void testCountContacts(){
        //List<Account> accountList = new List<Account>
        List<Account> accountList = RandomContactFactory.generateTestAccounts(5);
        insert accountList;
        List<Contact> contactList = RandomContactFactory.generateRandomContacts(5, 'Sharma');
        insert contactList;
        List<id> idList = new List<id>();
        //List<Account> accList = [Select id, Name from Account where name like 'Test%'];
        for(Account a: accountList){
            System.debug(a.id+' ,'+a.name);
            idList.add(a.Id);
        }
        //system.assert(false, '==>' + idList);
        Test.startTest();
        AccountProcessor.countContacts(idList);
        Test.stopTest();   
    }
}