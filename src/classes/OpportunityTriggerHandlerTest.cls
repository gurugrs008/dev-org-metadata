@isTest
private class OpportunityTriggerHandlerTest {
    private static Account acc;
    private static Opportunity opp;
    private static void createData(){
      acc = new Account();
      acc.Name = 'Test Account';
      insert acc;
      
      opp = new Opportunity();
      opp.Name = 'Test Opportunity';
      opp.AccountId = acc.Id;
      opp.StageName = 'Prospecting';
      opp.CloseDate = Date.today() - 1;
      insert opp;  
    }
    private static testMethod void beforeUpdateTriggerTest(){
        createData();
        opp.StageName = 'Closed Won';
        update opp;
        opp = [select id, CloseDate from Opportunity where id = :opp.Id];
        system.assertEquals(Date.today(), opp.CloseDate);
    }
}