global class AccountDynamicPicklist extends VisualEditor.DynamicPickList {
	global override VisualEditor.DataRow getDefaultValue(){
        VisualEditor.DataRow defaultValue = new VisualEditor.DataRow('', '');
        return defaultValue;
    }
    global override VisualEditor.DynamicPickListRows getValues() {
    	VisualEditor.DynamicPickListRows  myValues = new VisualEditor.DynamicPickListRows();
    	List<Account> accountList = [Select Id, Name from Account order by Name];
    	
    	for(Account acc : accountList) {
    		VisualEditor.DataRow value = new VisualEditor.DataRow(acc.Name, acc.Name);	
    		myValues.addRow(value);
    	}    	
    	
    	myValues.setContainsAllRows(true); 
        return myValues;
    }
}