/*******************************************************************************************************
*@author Appirio
*@date May 2018
*@description Global class which provides wrapper for chart components
*/

global with sharing class Demo_ChartController {
    
    global class ChartSource extends LADS.RecordBase{
        @AuraEnabled
        global List<Integer> data{get;set;}
        @AuraEnabled
        global List<String> labels{get;set;}
        @AuraEnabled
        global List<List<Integer>> points{get;set;}
        @AuraEnabled
        global String options{get;set;}
    }
    
    global class SortChartData implements comparable{
        global String key{get;set;}
        global Double value{get;set;}
        global Integer compareTo(Object compareTo) {
            SortChartData compareToRec = (SortChartData)compareTo;
            if (value > compareToRec.value) {
                return 1;
            } else if (value < compareToRec.value) {
                return -1;
            } else {
                return 0;
            }
        }
    }
    
    global Map<String, Double> getSortedMapByValues(Map<String, Double> sourceMap){
        Map<String, Double> sortedMap = new Map<String, Double>();
        List<SortChartData> mapData = new List<SortChartData>();
        for(String s: sourceMap.keySet()){
            SortChartData data = new SortChartData();
            data.key = s;
            data.value = sourceMap.get(s);
            mapData.add(data);
        }
        mapData.sort();
        
        for(SortChartData sortedRes : mapData){
            sortedMap.put(sortedRes.key,sortedRes.value);
        }
        return sortedMap;
    }
    
    @AuraEnabled
    global static map<String,String> getDivisionToColorMapping(){ 
        Map<String, String> mpDivisonToColorMapping = new Map<String, String>();
        for(GU360_Division_Color_Mapping__mdt divisonMap  : [SELECT Opportunity_Stage__c, Color__c FROM 
                                                             GU360_Division_Color_Mapping__mdt
                                                            ]){
            mpDivisonToColorMapping.put(divisonMap.Opportunity_Stage__c,divisonMap.Color__c);
        }
        return mpDivisonToColorMapping;
    }
    @AuraEnabled
    global static List<ChartSource> retrieveRecordSet() {      
      String accountId = '0012800000r69TvAAI';
      List<ChartSource> chartDataList = new List<ChartSource>();
      ChartSource chartData = new ChartSource();
      Demo_ChartController lc = new Demo_ChartController();
      Map<String, Double> mapStageToRevenue = new Map<String, Double>();
      List<Opportunity> opportunityList = [SELECT Id, ExpectedRevenue, StageName
          FROM Opportunity
          WHERE AccountId = :accountId];
        
      for (Opportunity opp : opportunityList) {
        if (!mapStageToRevenue.containsKey(opp.StageName)) {
          mapStageToRevenue.put(opp.StageName, 0);
        }
        mapStageToRevenue.put(opp.StageName, mapStageToRevenue.get(opp.StageName) + opp.ExpectedRevenue);
      }
      mapStageToRevenue = lc.getSortedMapByValues(mapStageToRevenue);
      //fill the wrapper
      chartData.labels = new List<String>();
      chartData.data  = new List<Integer>();
      chartData.options = '';
      for (String s : mapStageToRevenue.keySet()) {
        chartData.labels.add(s);
        chartData.data.add((Integer)mapStageToRevenue.get(s));
      }
      chartDataList.add(chartData);
      return chartDataList;
    }
}