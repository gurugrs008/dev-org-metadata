public class OpportunityHandler {
    public static void beforeUpdateTrigger(List<Opportunity> oppList, Map<Id, Opportunity> oldMap){
        for(Opportunity opp : oppList){
            if (opp.StageName != oldMap.get(opp.Id).StageName && (opp.StageName == 'Closed Won' || opp.StageName == 'Closed Lost')){
      			opp.CloseDate = date.today();
    		}
        }
    }
    
    public static void afterUpdateTrigger(List<Opportunity> oppList){
        for(Opportunity opp: oppList){
            if(opp.custom_status__C == 'Reset'){
                //delete all related products
            }
        }
        
    }
}