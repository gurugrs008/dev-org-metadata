global with sharing class LADS_SampleController {
    
    global class RecordCtrl implements LADS.EditableRecordDataProvider {
        global Sample retrieveRecord(String url) {
            Sample s = new Sample('THIS TEST');
            s.name = 'THIS IS A TEST';
            return s;	
        }	
        
        global Sample saveRecord(String url, String data) {
            return (Sample)JSON.deserialize(data, Sample.class);
        }	
    }
    
    global class RecordSetCtrl implements LADS.FilterableRecordSetDataProvider {
        global List<Sample> retrieveRecordSet(String url) {
            return this.retrieveFilteredRecordSet(url,null,null,null);	
        }

        global List<Sample> retrieveFilteredRecordSet(String url, List<LADS.FilterCriteria> filterCriteria,Integer resultOffset,Integer resultLimit) {
            List<Sample> samples = new List<Sample>();
            Sample s1 = new Sample('1');
            samples.add(s1);
            Sample s2 = new Sample('2');
            samples.add(s2);
            Sample s3 = new Sample('3');
            samples.add(s3);
            return samples;	
        }		     
        
    }

    global class EditableRecordSetCtrl implements LADS.EditableRecordSetDataProvider {
        global List<Sample> retrieveRecordSet(String url) {
            List<Sample> samples = new List<Sample>();
            Sample s1 = new Sample('1');
            samples.add(s1);
            Sample s2 = new Sample('2');
            samples.add(s2);
            Sample s3 = new Sample('3');
            samples.add(s3);
            return samples;	
        }	

        global Sample saveRecordSetItem(String url,String item) {
            return new Sample();
        }

        global Sample deleteRecordSetItem(String url,String item) {
            return new Sample();
        }        
    }
 
    global class Sample extends LADS.RecordBase {
        @AuraEnabled
        global String name{get;set;}
        @AuraEnabled
        global String sampleNumber{get;set;}
        @AuraEnabled
        global Boolean canManageRecord{get;set;}		
        
        global Sample() {
            new Sample('');
        }

        global Sample(String test) {
            this.name = test+'Test';	
            this.id = test+'ABC123';
            this.sampleNumber = test+'34552';
            this.canManageRecord = true;
        }
        
    }
}