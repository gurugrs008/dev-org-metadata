global class LeadProcessor implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(
            'Select Id,LeadSource from Lead'
        );
    }
    
    global void execute(Database.BatchableContext bc, List<Lead> leadScope){
        List<Lead> leadList=new List<Lead>();
        for(Lead ld:leadScope){
            ld.LeadSource = 'Dreamforce';
            leadList.add(ld);
        }
        update leadList;
    }
    global void finish(Database.BatchableContext bc){
        
    }

}