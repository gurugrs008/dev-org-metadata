@isTest
private class AddPrimaryContactTest {
    @isTest private static void testPrimaryContact(){
        List<Account> accountList = new List<Account>();
        for(Integer i=1;i<=50;i++){
            accountList.add(new Account(Name = 'Test'+i, BillingState = 'NY'));
            accountList.add(new Account(Name = 'TestAccount'+i,BillingState = 'CA'));
        }
        insert accountList;
        
        Contact co=new Contact();
        co.lastName = 'demo';
        String str = 'NY';
        AddPrimaryContact apc = new AddPrimaryContact(co,str);
        Test.startTest();
        System.enqueueJob(apc);
        Test.stopTest();
    }

}