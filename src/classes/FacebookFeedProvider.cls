global class FacebookFeedProvider implements SocialFeedController.SocialFeedProvider {
    global List<SocialFeedController.SocialPost> retrievePosts(String identifier,Integer postLimit) {
        List<SocialFeedController.SocialPost> socialPosts = new List<SocialFeedController.SocialPost>();
        try {
            System.debug('Inside FacebookFeedProvider.retrievePosts');
            String accessToken = retrieveFacebookBearerToken();
            HttpRequest req = new HttpRequest();
            //req.setEndpoint('https://graph.facebook.com/v2.8/georgetownuniv/?fields=username,name,picture');
            //req.setEndpoint('https://graph.facebook.com/v2.8/TestApp-Community-1-253290355243344/?fields=cover');
            req.setEndpoint('https://graph.facebook.com/v2.8/20531316728?fields=cover');
            System.debug('accessToken ==== '+accessToken);
            req.setHeader('Authorization','Bearer '+accessToken);
            req.setMethod('GET');
            Http http = new Http();
            HTTPResponse res = http.send(req);
            GU360_Wrappers.FBPageOverview fbPage = (GU360_Wrappers.FBPageOverview)JSON.deserialize(res.getBody(),GU360_Wrappers.FBPageOverview.class);
            String fbName = fbPage.name;
            String fbPic = fbPage.picture.data.url;
            String fbUn = '@'+fbPage.username;
            String fbLink = fbPage.link;
            req = new HttpRequest();
            //req.setEndpoint('https://graph.facebook.com/v2.8/20531316728/posts?fields=full_picture,created_time,picture,message,message_tags,link,name,type,id,permalink_url&limit='+postLimit);
            //req.setEndpoint('https://graph.facebook.com/v2.8/TestApp-Community-1-253290355243344/posts?fields=cover');
            //req.setEndpoint('https://graph.facebook.com/v2.8/20531316728/photos');
            req.setEndpoint('https://graph.facebook.com/v2.8/20531316728?fields=cover');
            req.setHeader('Authorization','Bearer '+accessToken);
            req.setMethod('GET');
            res = http.send(req);
            
            GU360_Wrappers.FBPostWrapper fbPostWrap = (GU360_Wrappers.FBPostWrapper)JSON.deserialize(res.getBody(),GU360_Wrappers.FBPostWrapper.class);
            for(GU360_Wrappers.FBPost post : fbPostWrap.data) {
                SocialFeedController.SocialPost sp = new SocialFeedController.SocialPost();
                sp.authorName = fbName;
                sp.authorPhotoUrl = fbPic;
                sp.authorUsername = fbUn;
                //sp.authorLink = post.permalink_url;//fbLink;
                sp.authorLink = 'https://www.facebook.com/TestApp-Community-1-253290355243344/';
                sp.content = post.message;
                
                //$Resource.GU360_Resources + '/img/sprites/media.svg#facebook';
                sp.feedTypeIconUrl = '/img/sprites/media.svg#facebook';
                sp.tweetOrFbUrl = 'https://www.facebook.com/TestApp-Community-1-253290355243344';
                //2017-04-22T13:05:00+0000
                List <String> stringParts = post.created_time.split('T');
                List <String> dateParts = stringParts[0].split('-');
                List <String> timeParts = stringParts[1].split('\\+')[0].split(':');
                DateTime yourDateVariable = DateTime.newInstanceGmt(Integer.valueOf(dateParts[0]), 
                                                                    Integer.valueOf(dateParts[1]), 
                                                                    Integer.valueOf(dateParts[2]), 
                                                                    Integer.valueOf(timeParts[0]), 
                                                                    Integer.valueOf(timeParts[1]), 
                                                                    Integer.valueOf(timeParts[2]));
                sp.postDate = yourDateVariable;
                sp.relativePostDateString = getRelativeDate(sp.postDate);
                if(!String.isBlank(post.full_picture)) {
                    sp.hasPhoto = true;
                    sp.mediaUrl = post.full_picture;
                }
                socialPosts.add(sp);
            }
            return socialPosts;
        }
        catch(Exception e){
            //SystemLogUtility.error(e);
            throw new AuraHandledException(e.getMessage());
        }
    }
    public static String retrieveFacebookBearerToken() {
        Facebook_API_Authentication_Settings__c fbApi = Facebook_API_Authentication_Settings__c.getOrgDefaults();
        try {
            System.debug('retrieveFacebookBearerToken == ');
            HttpRequest req = new HttpRequest();
            System.debug('fbApi.Consumer_Key__c == '+fbApi.Consumer_Key__c);
            System.debug('fbApi.Consumer_Secret__c == '+fbApi.Consumer_Secret__c);
            req.setEndpoint(' https://graph.facebook.com/v2.5/oauth/access_token?client_id='+fbApi.Consumer_Key__c+'&client_secret='+fbApi.Consumer_Secret__c+'&grant_type=client_credentials');
            req.setMethod('GET');
            Http http = new Http();
            HttpResponse res = http.send(req);
            GU360_Wrappers.FacebookBearerResponse fbr = (GU360_Wrappers.FacebookBearerResponse)JSON.deserialize(res.getBody(),GU360_Wrappers.FacebookBearerResponse.class);
            System.debug('access token == '+fbr.access_token);
            return fbr.access_token;
        }
        catch(Exception e){
            //SystemLogUtility.error(e);
            throw new AuraHandledException(e.getMessage());
        }
        
    } 
    
    private static String getRelativeDate(DateTime dt) {
        String relativeDate = '';
        String unit = '';
        Long nowLong = DateTime.now().getTime();
        Long thenLong = dt.getTime();
        Long elapsedSeconds = (nowLong - thenLong)/1000;
        if(elapsedSeconds > 60) {
            Long elapsedMinutes = elapsedSeconds/60;
            if(elapsedMinutes > 60) {
                Long elapsedHours = elapsedMinutes/60;
                if(elapsedHours > 24) {
                    Long elapsedDays = elapsedHours / 24;
                    if(elapsedDays > 28) {
                        Long elapsedMonths = elapsedDays / 28;
                        relativeDate = Integer.valueOf(elapsedMonths) + ' months ago';
                    }
                    else {
                        relativeDate = Integer.valueOf(elapsedDays) + ' days ago';
                    }
                }
                else {
                    relativeDate = Integer.valueOf(elapsedHours) + ' hours ago';
                }
            }  
            else {
                relativeDate =  Integer.valueOf(elapsedMinutes) + ' minutes ago'; 
            }
        }
        else {
            relativeDate = Integer.valueOf(elapsedSeconds) + ' seconds ago';               
        }
        
        return relativeDate;
    }
}