({
    doInit : function(component, event, helper) {
        //retrieve social provider metadata api names
        
        //check if any are currently disabled by user (in future...)
        
        //pass retrieved provider class names to retrievePosts method
        
        //retrievPosts method should iterate list fo names and run retrievePosts method then sort all results together chronologically and return a single list
        var isLoadInBackground = component.get('v.LoadinBackground');
        var action = component.get("c.retrievePosts");
        
        action.setParams({ socialFeedApiName : component.get("v.socialFeedApiName") });
        if(isLoadInBackground){
            action.setBackground();
		}
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            // This callback doesn’t reference cmp. If it did,
            // you should run an isValid() check
            //if (cmp.isValid() && state === "SUCCESS") {
            console.log('state == '+state);
            
            if (state === "SUCCESS") {
                component.set('v.socialPosts',response.getReturnValue());
            }		
            //var stencil = component.find('stencil');
            //if(stencil) stencil.hideStencil();		
        });
        $A.enqueueAction(action);
        
        // For feedOptions
        var retrieveOptions = component.get("c.retrieveSourceOptions");
        retrieveOptions.setParams({ socialFeedApiName : component.get("v.socialFeedApiName") });
        retrieveOptions.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.feedOptions',response.getReturnValue());
            }		
        });
        $A.enqueueAction(retrieveOptions);
        
    },
    toggleSocialOptions : function(component, event, helper) {
        var editComp = component.find('social-edit');
        var readComp = component.find('social-read');
        $A.util.toggleClass(readComp,'slds-hide');
        $A.util.toggleClass(editComp,'slds-hide');
    },
    saveSocialOptions : function(component, event, helper) {
        var editComp = component.find('social-edit');
        var readComp = component.find('social-read');
        $A.util.toggleClass(readComp,'slds-hide');
        $A.util.toggleClass(editComp,'slds-hide');
    }
})