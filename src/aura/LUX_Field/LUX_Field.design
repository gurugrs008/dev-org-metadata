<design:component label="LUX List">
    
    <design:attribute name="componentUniqueName" label="Unique Name" description="Unique key that can be referenced in hash state" />
    <design:attribute name="label" label="Label Text" description="Text to show for field label."/>
    <design:attribute name="showLabel" label="Show Label?" description="If checked Label will be displayed." />
	<!--
    <design:attribute name="labelStyle" label="Label Style" description="Set to None to hide.  Selecting either of the other options will make label show."/>
	-->
	<design:attribute name="labelPosition" label="Label Position" description="Options include: None, Top, Inline, and Both" default="Top"/>
    
    <design:attribute name="dataProviderApiName" label="Data Provider"  description="Name of apex class that will handle data for this component. (e.g. LADS_SampleProviderName.InnerClassName)" /> <!-- datasource="apex://LADS_RecordDataProviderPicklist" -->
    <design:attribute name="propertyApiName" label="Data Provider Property" description="Name of the desired property on the return type of the selected data provider. (e.g. firstName)"/>  
    <design:attribute name="fieldType" label="Field Type" datasource="Text,TextArea,Picklist,RichText,URL,Boolean,Date,DateTime,Currency,Blank" />
    <design:attribute name="urlApiName" label="URL Field Name" description="If Field Type is URL, the Property value above is used to store the website name and the property specified here will be used to store the URL itself." />
    <design:attribute name="picklistObjectApiName" label="Picklist Object API Name" description="If Field Type is Picklist, this is the API name of the Salesforce Object you wish to use a field from to populate values. (e.g. Contact)"/>
    <design:attribute name="picklistFieldApiName" label="Picklist Field API Name" description="If Field Type is Picklist, this is the API of a Picklist type field from the Object specified above.  It will be used to populate options within picklist." />  
    <design:attribute name="readOnlyField" label="Read-Only" description="If checked, field will not be able to be edited in the community."/>
    <design:attribute name="maxLength" label="Max length for Rich Text and Text Area fields types.  Up to 100000 characters are supported." />
    <design:attribute name="fieldStyle" label="Field Style" datasource="Inline,Block" description="Field style to render.  Inline has icons for saving.  Block uses buttons that only disappear once clicked."/>
    <design:attribute name="showTooltip" label="Show Tooltip" description="If checked a help text icon will appear which will show the text specified below when hovered."/>    
    <design:attribute name="tooltipText" label="Tooltip Text" description="Tooltip text to show on hover of icon detailed above."/>
    <design:attribute name="placeholder" label="Placeholder Text" description="Text to show in field when empty. (e.g. Enter first name here)"/>    


    
    <design:attribute name="containerClassName" label="Container CSS Class" description="Name of CSS class to append to outer container Div of field."/>

    <!--
	<design:attribute name="useTheme" label="Use Theme" description="This should be enabled when used within a Community using a custom theme."/>
	-->
    <design:attribute name="hideWhenBlank" label="Hide When Blank" description="If enabled this component will be hidden to viewers without edit permission"/>
    <design:attribute name="showDynamicInlineComp" label="Show Custom Inline Component" description="If enabled the component given by the field below will be rendered below the field itself." />
    <design:attribute name="dynamicInlineCompName" label="Custom Inline Component Name" description="Developer name of the Lightning component that should be rendered below the field if above field is checked." />
	<design:attribute name="inlineComponentParams" label="Custom Inline Component Parameters" description="Comma separated ‘key’ : ‘value’ pairs of parameters to be passed in to the inline component. e.g. ‘firstName’ : ‘Jane’, ‘lastName’ : ‘Doe’ , ..."/>
	
    <design:attribute name="useDynamicVisibility" label="Enable Dynamic Visibility" description="If enabled, this field will only be visible if the property specified by Visibility API Name is true."/>
    <design:attribute name="visibilityApiName" label="Visiblity API Name" description="Name of a Boolean property on data provider that should determine whether field is visible when enabled." />  
    
    <design:attribute name="contextApiName" label="Context API Name" description="Name of a Boolean property on data provider that should determine whether field is editable when enabled." />
    <design:attribute name="batchRequests" label="Enable Request Batching" description="Requires LADS Cache component in Community theme." />  
    <design:attribute name="fetchInBackground" label="Fetch In Background" description="If enabled data will be retrieved using background actions.  This allows users to keep browsing app during load but may cause loading to take longer." />      
</design:component>