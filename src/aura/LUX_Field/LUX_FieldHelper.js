({
    renderInlineInputComponent : function(component) {
        //@todo implement this rendering if necessary
    },
    
    toggleListExpanded : function(component, event, helper) {
        var checked = component.get('v.isListExpanded');
        component.set('v.isListExpanded',!checked);
    },
    toggleInlineSelectEdit : function (component) {
        component.set('v.selectIsUnsaved',!component.get('v.selectIsUnsaved'));
    },  
    handleComponentRendering : function(component) {
        var helper = this;
        var data = component.get('v.data');
                
        var compType = component.get('v.fieldType');          
        var contextApiName = component.get('v.contextApiName');
        var visibilityApiName = component.get('v.visibilityApiName');
                
        if(!$A.util.isUndefinedOrNull(contextApiName)) component.set('v.canManageRecord',data[contextApiName]);
        if(!$A.util.isUndefinedOrNull(visibilityApiName)) component.set('v.useDynamicVisibility',!data[visibilityApiName]);
        
        var val = data[component.get('v.propertyApiName')];
        if(!$A.util.isEmpty(val)) {
            component.set('v.isBlank',false);
            
            if(compType.toLowerCase() === 'text' || compType.toLowerCase() === 'textarea' || compType.toLowerCase() === 'richtext' || compType.toLowerCase() === 'picklist' || compType.toLowerCase() === 'url') {
                component.set('v.stringFieldValue',val);
            } else if(compType.toLowerCase() === 'date' || compType.toLowerCase() === 'datetime') {
                try {
                    component.set('v.dateFieldValue', new Date(val).toISOString().substr(0, 10));
                } catch(e) {
                    
                }
            } else if(compType.toLowerCase() === 'boolean') {
                // This is incomplete now as Inline css dosen't support Boolean. need to work on it
                component.set('v.booleanFieldValue', val);
                component.set('v.stringFieldValue', val); //Use for block type Boolean
            } else if(compType.toLowerCase() === 'currency'){
                
                component.set('v.currencyFieldValue',val);
            }
        } else {
            component.set('v.isBlank',true);
            if(compType.toLowerCase() === 'text' || compType.toLowerCase() === 'textarea' || compType.toLowerCase() === 'richtext' || compType.toLowerCase() === 'currency') {
                var inlineInput = component.find('inline-input-field');
                $A.util.addClass(inlineInput,'pristine');
            }
            
        }
        var propertyNameURL = component.get("v.urlApiName");
        
        component.set('v.lastSavedStringFieldValue',component.get("v.stringFieldValue"));
        if(!$A.util.isUndefinedOrNull(propertyNameURL)) {
            component.set('v.stringFieldValue2',data[component.get("v.urlApiName")]);
            component.set('v.lastSavedStringFieldValue2',component.get("v.stringFieldValue2"));
        }
        
        //Populate visibility value
        var showVisibilityControls = component.get('v.showVisibilityControls');
        var visibilityIndex = component.get('v.visibilityIndex');
        if(showVisibilityControls){
            var mapFieldVisibility = data['mapFieldVisibility'];
            if(!$A.util.isUndefinedOrNull(mapFieldVisibility)
               && !$A.util.isUndefinedOrNull(visibilityIndex)){
                var visibVal = mapFieldVisibility[visibilityIndex];
                if(!$A.util.isUndefinedOrNull(visibVal)){
                    component.set('v.visibilityVal', visibVal);
                }
            }
        }
        //render inline input
        var showInlineFieldComp = component.get('v.showDynamicInlineComp');
        
        var compsToGen = [];
        var compToGen = [];
        var compType = component.get('v.dynamicInlineCompName');
        compToGen.push(compType);
        var props = {};
        props.data = component.get('v.data');
        props.dataProviderApiName = component.get('v.dataProviderApiName');
        props.readOnlyField = component.get('v.readOnlyField');
        props.propertyName = component.get('v.propertyApiName');
        props.componentType = component.get('v.fieldType');
        props.inBuilder = component.get('v.inBuilder');
        compToGen.push(props);
        compsToGen.push(compToGen);
        if(compsToGen.length > 0 && showInlineFieldComp === true) {
            
            $A.createComponents(compsToGen, function (cmps, status, error) {
                if(status === 'SUCCESS') component.set('v.dynamicInlineComp',cmps);
            });
        }
      
    },
    synchronizeData : function(component,data, event) {
        if(component.isValid()) {
            var recordProvider = component.find('data-provider');
            recordProvider.syncData();
        }
    }    
})