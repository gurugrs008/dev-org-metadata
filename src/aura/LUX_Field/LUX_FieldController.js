({
    doInit : function(component, event, helper) {
        //create component
        var compName = 'c:LADS_RecordFieldInner';
        $A.createComponent(compName,{
            "label" : component.get('v.label'),
            "showLabel" : component.get('v.showLabel'),
            "labelStyle" : component.get('v.labelStyle'),
            "labelPosition" : component.get('v.labelPosition'),
            "dataProviderApiName" : component.get('v.dataProviderApiName'),
            "propertyName" : component.get('v.propertyName'),
            "componentType" : component.get('v.fieldType'),
            "PropertyNameURL" : component.get('v.urlApiName'),
            "objectAPIName" : component.get('v.picklistObjectApiName'),
            "fieldAPIName" : component.get('v.fieldAPIName'),
            "readOnlyField" : component.get('v.readOnlyField'),
            "maxLength" : component.get('v.maxLength'),
            "fieldStyle" : component.get('v.fieldStyle'),
            "showTooltip" : component.get('v.showTooltip'),
            "tooltipText" : component.get('v.tooltipText'),
            "placeholder" : component.get('v.placeholder'),
            "showVisibilityControls" : false,
            "visibilityIndex" : '',
            "containerClassName" : component.get('v.containerClassName'),
            "useTheme" : false,
            "hideWhenBlank" : component.get('v.hideWhenBlank'),
            "showDynamicInlineComp" : component.get('v.showDynamicInlineComp'),
            "dynamicInlineCompName" : component.get('v.dynamicInlineCompName'),
            "hideOnPersona" : component.get('v.useDynamicVisibility'), 
            "visibilityApiName" : component.get('v.visibilityApiName'),
            "contextApiName" : component.get('v.contextApiName'),
            "batchRequests" : component.get('v.batchRequests'),
            "fetchInBackground" : component.get('v.fetchInBackground')
        },
        function(newCmp,status,errorMessage) {
            if (status === "SUCCESS") {
                var body = component.get("v.body");
                body.push(newCmp);
                component.set("v.body", body);
            }
            else if (status === "INCOMPLETE") {
                console.error("No response from server or client is offline.")
                // Show offline error
            }
            else if (status === "ERROR") {
                console.error("Error: " + errorMessage);
                // Show error message
            }
        });
    }

})