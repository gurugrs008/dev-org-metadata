({
    doInit : function(component, event, helper) {
        //create component
        var compName = 'c:LADS_RecordSetListInner';
        $A.createComponent(compName,{
            "componentUniqueName" : component.get('v.componentUniqueName'),
            "dataProviderApiName" : component.get('v.dataProviderApiName'),
            "contextProviderApiName" : component.get('v.contextProviderApiName'),
            "canManagePropertyName" : component.get('v.canManagePropertyName'),
            "dynamicRowEditProperty" : component.get('v.dynamicRowEditProperty'),
            "itemComponentTag" : component.get('v.itemComponentTag'),
            "showHeader" : component.get('v.showHeader'),
            "heading" : component.get('v.heading'),
            "headerStyle" : component.get('v.headerStyle'),
            "headerLinkUrl" : component.get('v.headerLinkUrl'),
            "showHeaderIcon" : component.get('v.showHeaderIcon'),
            "headerIconUrl" : component.get('v.headerIconUrl'),
            "useLADSIcon" : component.get('v.useSLDSIcon'),
            "ladsIconName" : component.get('v.sldsIconName'),
            "showTooltip" : component.get('v.showTooltip'),
            "tooltipText" : component.get('v.tooltipText'),
            "tooltipLength" : component.get('v.tooltipLength'),
            "tooltipPos" : component.get('v.tooltipPos'),
            "isCollapsible" : component.get('v.isCollapsible'),
            "collapsedOnLoad" : component.get('v.collapsedOnLoad'),
            "autoCollapseMin" : component.get('v.autoCollapseMin'),
            "isReadOnly" : component.get('v.isReadOnly'),
            "addButtonLocation" : component.get('v.addButtonLocation'),
            "showNewEntriesatTop" : component.get('v.showNewEntriesatTop'), 
            "addLabel" : component.get('v.addLabel'),
            "maxListSize" : component.get('v.maxListSize'),
            "showListCount" : component.get('v.showListCount'),
            "enableLoadMore" : component.get('v.enableLoadMore'),
            "currentLimit" : component.get('v.currentLimit'),
            "wrapperClass" : component.get('v.wrapperClass'),
            "listClass" : component.get('v.listClass'),
            "hideWhenBlank" : component.get('v.hideWhenBlank'),
            "showWhenBlank" : component.get('v.showEmptyMessage'),
            "showBlankMessage" : component.get('v.showBlankMessage'),
            "batchRequests" : component.get('v.batchRequests'),
            "fetchInBackground" : component.get('v.fetchInBackground'),            
            "hideFromAdminWhenBlnk" : component.get('v.hideFromAdminWhenBlnk')                       
        },
        function(newCmp,status,errorMessage) {
            console.log(status,errorMessage,newCmp);
            if (status === "SUCCESS") {
                var body = component.get("v.body");
                body.push(newCmp);
                component.set("v.body", body);
            }
            else if (status === "INCOMPLETE") {
                console.error("No response from server or client is offline.")
                // Show offline error
            }
            else if (status === "ERROR") {
                console.error("Error: " + errorMessage);
                // Show error message
            }
        });
    }

})