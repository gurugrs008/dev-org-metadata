<design:component label="LADS List" >
    <design:attribute name="componentUniqueName" label="Unique Name" description="Unique key that can be referenced in hash state" />
	<design:attribute name="dataProviderApiName" label="Data Provider" description="Name of apex class that will handle data for this component. (e.g. LADS_SampleProviderName.InnerClassName)" /> <!-- datasource="apex://LADS_RecordSetDataProviderPicklist" -->
    <design:attribute name="contextProviderApiName" label="Context Data Provider" description="Name of apex class that has a Boolean property that can be used to determine if current use to add/remove/edit items from the List." /> <!-- datasource="apex://LADS_RecordDataProviderPicklist" -->
    <design:attribute name="canManagePropertyName" label="Can Manage Property" description="Name of property from context provider class above that is used to determine whether user can manage list."/>
    <design:attribute name="dynamicRowEditProperty" label="Row Editability Property" description="Name of property from data provider class above that is used to determine whether each row can be edited or not.  If left blank all rows inherit editability from the above Can Manage Property value.  If Read-Only is true this property is ignored." />
    <design:attribute name="itemComponentTag" label="List Item Component Tag" description="Full tag of Lightning component to render for each item in retrieved data. (e.g. c:LADS_SampleListItemComponent)" default="c:"/>    
    <design:attribute name="showHeader" label="Show Header?" description="If checked heading will be displayed."/>
    <design:attribute name="heading" label="Heading Text" description="Text to show in heading."/>
    <design:attribute name="headerStyle" label="Header Style" datasource="H3,Link,Div" default="Div" description="Style of heading to show."/>
    <design:attribute name="headerLinkText" label="Header Link Text" description="If header style is set to Link, this determines the text to show for the link."/>
    <design:attribute name="headerLinkUrl" label="Header Link Url" description="If header style is set to Link, this determines the text to URL that the link navigates to."/>
    <design:attribute name="showHeaderIcon" label="Show Header Icon?" description="If checked the icon specified by the URL below will be shown within the heading."/>
    <design:attribute name="headerIconUrl" label="Header Icon Url" description="Required if Show Header Icon is checked. Specifies url of icon to display."/>
    <design:attribute name="useSLDSIcon" label="Use Lightning Design System Icon"  default="false" />
    <design:attribute name="sldsIconName" label="SLDS Icon Name"/>

    <design:attribute name="showTooltip" label="Show Tooltip" description="If checked a help text icon will appear which will show the text specified below when hovered."/>    
    <design:attribute name="tooltipText" label="Tooltip Text" description="Tooltip text to show on hover of icon detailed above."/>
    <design:attribute name="tooltipLength" label="Tooltip size" description="The size of the tooltip. Options include small, medium, large, Xlarge"/>
    <design:attribute name="tooltipPosition" label="Tooltip Position" description="The position of the tooltip. Options include up, down, left, right"/>
    
    <design:attribute name="isCollapsible" label="Collapsible?" description="If checked, list is collapsible and will only load items once expanded."/>
    <design:attribute name="collapsedOnLoad" label="Collapsed on Load" description="If checked and collapsible is enabled, list loads collapsed.  This improves load performance."/> 
    <design:attribute name="autoCollapseMin" label="Auto-Collapse Minimum" description="Lists above this size will automatically collapse. Set to -1 to disable auto-collapse." />
    
    <design:attribute name="isReadOnly" label="Read-Only?" description="If checked, users will be unable to add, remove, or edit items.  This overrides context provider settings specified above."/>
    <design:attribute name="addButtonLocation" label="Add Button Location" description="Specifies where to show Add Button" datasource="Header,Bottom" />
    <design:attribute name="showNewEntriesatTop" label="Show New Entries at Top" description="If checked when the add button is clicked, the new item will appear at top of list instead of at the end."/>
    <design:attribute name="addButtonLabel" label="Add Button Text" default="Add" description="Text to show on the add button."/>
    <design:attribute name="maxListSize" label="Max List Size" description="Max size of list to support.  If length grows beyond this add button will disappear."/>
    <design:attribute name="showListCount" label="Show Count?" description="If checked the heading will include an indicator with the total number of the items in the list."/>
    <design:attribute name="enableServerLoadMore" label="Enable Load More" description="If checked the list will only partial load the list items and allow paging through results.  This is primarily used for search results. Note that data provider must support filterable lists for this to function properly - i.e. Server-side pagination."/>
    <design:attribute name="initialLoadLimit" label="Initial Amount to Load" description="When Enable Load More is checked, this determines number of items to load initially. Server-side pagination." />
    <design:attribute name="wrapperClass" label="Wrapper Container CSS Class" description="CSS class to append to list wrapper element."/>
    <design:attribute name="listClass" label="List Container CSS Class" description="CSS class to append to list item component iteration element."/>
    <design:attribute name="hideWhenBlank" label="Hide When Blank" default="True" description="Hide Component when the data is blank"/>
    <design:attribute name="showEmptyMessage" label="Show Empty Message" default="false" description="Show message to user when list is empty."/>
    <design:attribute name="emptyMessageText" label="Empty Message" description="This text will show when Show Empty Message is enabled and a list has no items." />
    <design:attribute name="batchRequests" label="Enable Request Batching" description="Requires LADS Cache component in Community theme." />
    <design:attribute name="fetchInBackground" label="Fetch In Background" description="If enabled data will be retrieved using background actions.  This allows users to keep browsing app during load but may cause loading to take longer." />    
	
</design:component>