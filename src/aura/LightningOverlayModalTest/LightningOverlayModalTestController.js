({
  showModal : function(cmp, evt, helper) {
    component.find('overlayLib').showCustomModal({
      //modal attributes
    }).then(function (overlay) {
      component.set('v.overlayPanel', overlay);
    });
  },
  handleOk : function(cmp, evt, helper) {
    var overlayPanel = component.get('v.overlayPanel');
    overlayPanel[0].close();
  },
  toggleVisibility : function(component, event, helper) {
        var visible = component.get("v.visible");
        if(visible === true) {
            helper.hideModal(component);
            //run close action if provided and hiding modal
            if(component.get('v.closeAction') != null) {
                $A.enqueueAction(component.get('v.closeAction'));
            }
        } else {
            helper.launchModal(component);
        }
  }
})