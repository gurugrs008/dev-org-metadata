({
	launchModal : function(component, event, helper) {
		var overlayLibrary = component.find('overlayLib');
		var modalBody;
		//var modalBodyComp = component.get('v.modalBodyComponentName');
        var modalBodyComp = "c:modalContent";
        var modalBodyProps = {};
		var header = component.get('v.header');
		var footerProps = component.get('v.modalFooterProperties'); 
		var modalClass = component.get('v.modalClass');
        $A.createComponent(modalBodyComp, modalBodyProps,
                           function(content, status) {
                               if (status === "SUCCESS") {
                            	   console.log('inside success');
                                   modalBody = content;
                                   overlayLibrary.showCustomModal({
                                       header: header,
                                       body: modalBody, 
                                       showCloseButton: true,   
                                       cssClass: "mymodal",                                    
                                       closeCallback: function() {
                                           
                                       }
                                   })
                                   
                               }
                               
                           });
		
	}
})