({
	doInit : function(component, event, helper){
		component.set("v.initialURL", window.location.href);
    },
    onLocationChanged : function(component, event, helper){
        var initialUrl = component.get('v.initialURL');
        var currentUrl = window.location.href;
        console.log('initial Url ===== '+initialUrl);
        console.log('current Url ===== '+currentUrl);
    }
})