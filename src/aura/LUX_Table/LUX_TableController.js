({
    doInit : function(component, event, helper) {
        //create component
        var compName = 'c:LUX_TableInner';
        $A.createComponent(compName,{
            "dataProviderApiName" : component.get('v.dataProviderApiName'),
            "tableApiName" : component.get('v.tableApiName')
        },
        function(newCmp,status,errorMessage) {
            if (status === "SUCCESS") {
                var body = component.get("v.body");
                body.push(newCmp);
                component.set("v.body", body);
            }
            else if (status === "INCOMPLETE") {
                console.error("No response from server or client is offline.")
                // Show offline error
            }
            else if (status === "ERROR") {
                console.error("Error: " + errorMessage);
                // Show error message
            }
        });
    }

})