({
  doInit : function(component, event, helper) {
    var curUrl = window.location.href;
    console.log(curUrl);
    if(curUrl.indexOf('/flexipageEditor/') > -1) {
        window.setTimeout($A.getCallback(function() {
          var toggleButton = component.find('expandButton');
          $A.util.toggleClass(toggleButton, 'active');
        }),2000);
    }
  },
  togglePanel : function(component, event, helper){
    var toggleButton = component.find('expandButton');
    $A.util.toggleClass(toggleButton, 'active');
  },
  syncName : function(component, event, helper) {
    var data = component.get('v.accountData');
    console.log(JSON.stringify(data));
    component.set('v.accountName',data.accountName);
  }
})