({
    renderIcon: function(component) {
        //delete old comp if exists
        var compId = 'svg'+component.getGlobalId();
        var svgComp = document.getElementById(compId);

        var isImg = component.get('v.useImgTag');
        var assistiveText = component.get('v.assistiveText');
        var ariaHidden = component.get('v.ariaHidden');
        var className = component.get('v.class');
        var imgWidth = component.get('v.imgWidth');
        var imgHeight = component.get('v.imgHeight');

        var container;
        if(!isImg) {
            var svgns = "http://www.w3.org/2000/svg";
            var xlinkns = "http://www.w3.org/1999/xlink";

            var svgroot = document.createElementNS(svgns, "svg");
            // Add an "href" attribute (using the "xlink" namespace)
            var shape = document.createElementNS(svgns, "use");
            shape.setAttributeNS(xlinkns, "href", component.get("v.url"));
            if(!$A.util.isUndefinedOrNull(className)) {
                svgroot.setAttribute("class", className);
            }
            svgroot.setAttribute("id",compId);
            svgroot.setAttribute("aria-hidden", ariaHidden);
            svgroot.setAttribute("alt", assistiveText);
            svgroot.setAttribute("width", imgWidth);
            svgroot.setAttribute("height", imgHeight);
            svgroot.appendChild(shape);

            container = component.find("container").getElement();
            container.insertBefore(svgroot, container.firstChild);
        } else {
            var img = document.createElement('img');
            img.setAttribute('src',component.get('v.url'));
            img.setAttribute('alt',assistiveText);
            container = component.find("container").getElement();
            container.insertBefore(img, container.firstChild);
        }
    }
})