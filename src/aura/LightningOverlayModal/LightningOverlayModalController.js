({    
    handleShowModalFooter : function (component, event, helper) {        
        var modalBody;
        var modalFooter;
        var modalBodyMessage = component.get('v.modalBodyMessage');
        var modalBodyProperties = {};        
        modalBodyProperties.message = modalBodyMessage;
        try{
            $A.createComponents([
                ["c:modalContent",modalBodyProperties],
                ["c:modalFooter",{}]
            ],
                                function(components, status){
                                    if (status === "SUCCESS") {                                        
                                        modalBody = components[0];
                                        modalFooter = components[1];
                                        var overLayLib = component.find("overlayLib");
                                        overLayLib.showCustomModal({
                                            header: "Application Confirmation",
                                            body: modalBody, 
                                            footer: modalFooter,
                                            showCloseButton: true,
                                            cssClass: "my-modal,my-custom-class,my-other-class",
                                            closeCallback: function() {
                                                
                                            }
                                        })
                                    }
                                }
                               );
        }
        catch(err){
            console.log(err.message());
        }
    }
})