({
    "doInit" : function(cmp,event,helper){
        var action = cmp.get("c.serverEcho");
        action.setParams({ firstName : cmp.get("v.firstName") });
		action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {                
                console.log("From server: " + response.getReturnValue());
                alert("From server: " + response.getReturnValue());
            }                        
        });
        $A.enqueueAction(action);
    },
    "echo" : function(cmp,event,helper) {
        var action = cmp.get("c.serverEcho");
        action.setParams({ firstName : cmp.get("v.firstName") });
		action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("From server: " + response.getReturnValue());
            }                        
        });
        $A.enqueueAction(action);
        //console.log('Hello, this is under enqueue action');
    }
})