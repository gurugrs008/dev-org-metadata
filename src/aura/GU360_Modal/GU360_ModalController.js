({
	doInit : function(component, event, helper) {
    	var cls = component.get('v.class');
        if(cls != null && cls.length > 0) {
            var modal = component.find('gu-modal');
            $A.util.addClass(modal,cls);
        }
    },
    toggleVisibility : function(component, event, helper) {

	    var visible = component.get('v.visible');
        var modal = component.find('gu-modal');
        var backdrop = component.find('backdrop');
        visible = !visible;
        component.set('v.visible',visible);
        $A.util.toggleClass(modal,"slds-fade-in-open");
        $A.util.toggleClass(backdrop,"slds-backdrop--open");
        //broken?
        if(component.get('v.closeAction') != null) {
            $A.enqueueAction(component.get('v.closeAction'));
        }
        
	},
    
    manageClick : function(component, event, helper) {
		var appEvent = $A.get("e.c:GU360_ColumnHeaderClick");
        var grid = component.get("v.grid");
        //var columnIndex = component.get("v.columnIndex");
        //console.log("----columnIndex",columnIndex);
        console.log("----coldromgrid",grid);
        appEvent.setParams({
            "isColumnHeaderClick" : false,
            "grid" : grid
            
        });
        appEvent.fire();
	}
})