({
    doInit : function(component, event, helper) {	
        var shouldBatch = component.get('v.batchRequests');
        if(!shouldBatch) {
            helper.initializeComponent(component);
        } else {
            var appEvent = $A.get("e.c:LADS_DataRequest");
            appEvent.setParams({
                "dataProviderApiName" : component.get('v.dataProviderApiName'),
                "providerType" : "RecordSet",
                "useBackground" : component.get('v.fetchInBackground')
            });
            appEvent.fire();
                      
        }
    },
    onRefreshData : function(component, event, helper) {
        helper.initializeComponent(component,true);
    },
    loadMoreRows : function(component, event, helper) {
    	var params = event.getParam('arguments');
        if(params) {
        	var loadingLimit = params[1];
        	component.set('v.loadLimit',loadingLimit);
        }
        helper.loadMore(component);
    },
    onRecordSetDataChanged : function(component, event, helper) {
        //should update?
        var identifier = event.getParam('identifier');
        var action = event.getParam('action');
        var dataProviderApiName = event.getParam('dataProviderApiName');
        var data = event.getParam('data');
        if(action === 'update' && identifier !== component.getGlobalId() && component.get('v.dataProviderApiName') === dataProviderApiName) {
            helper.recordSetDataChanged(component,data);		
        }
        
    },
    
    onFilterRecordSetMethod : function(component, event, helper) {
        
        var params = event.getParam('arguments');
        var data = component.get('v.initdata');
        if (params) {
            var className = component.get('v.dataProviderApiName');
            var filters = params.filters;
            var filterType = params.filterType;
            var resultLimit = params.recordLimit;
            var resultOffset = params.recordOffset;
            var cb;
            if(params.callback) cb = params.callback;
            helper.retrieveFilteredRecordSet(component,filters,filterType,resultLimit,resultOffset,data,cb);
        }
        
    },
    handleDataResponseEvent : function(component, event, helper) {
        //Check for whether response providers matches either data or context
        //provider for this component and set variables from response if so.    
        var params = event.getParams();

        Object.keys(params.recordSetData).forEach(function(key) { //Record Set (Data)
            if(key == component.get('v.dataProviderApiName')){
                component.set('v.targetRecordSet', params.recordSetData[key]);
            }
        });
    }    
})