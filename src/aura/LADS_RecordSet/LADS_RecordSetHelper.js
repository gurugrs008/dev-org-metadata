({
    recordSetDataChanged : function(component,data){
        
        component.set('v.data',data);		
    },
    doneLoading : function(component) {
        var stopFn = component.get('v.dataLoadedAction');		
        if(stopFn) $A.enqueueAction(stopFn);
    },	
    initializeComponent : function(component,forceRefresh){
        var helper = this;
        
        var className = component.get('v.dataProviderApiName');
        var useBg = component.get('v.fetchInBackground');
        var url = window.location.href;
		window.LADS.retrieveRecordSetByUrlCB(component,className,url,forceRefresh,useBg,function(ret) {
            component.set('v.targetRecordSet',ret);
            component.set('v.initdata',ret);
            helper.doneLoading(component); 
            
            //@todo is this event necessary?
            var initDataLoadEvent = $A.get("e.c:LADS_RecordSetFilterDataEvent");
            initDataLoadEvent.setParams({
                "dataLoaded": true
            });
            initDataLoadEvent.fire();     	
        });	
        
    }, 
    synchronizeData : function(component,data) {
        var helper = this;
        var className = component.get('v.dataProviderApiName');             
        var syncPromise = window.LADS.saveRecordSetItem(component,className,data);
        
        try {
            syncPromise.then(
                $A.getCallback(function(result) {
                    component.set('v.targetRecordSet',result);
                }),
                $A.getCallback(function(error) {
                    //handle error
                    console.error(error);
                })
            );
        } catch(e) {
            console.error(e);
        }
    }, 
    retrieveFilteredRecordSet : function(component,filters,filterType,resultLimit,resultOffset,addToData,callback) {
        var helper = this;
        var retrieveFilteredResultProm = helper.retrieveFilteredRecordSetProm(component,filters,filterType,resultLimit,resultOffset);
        
        retrieveFilteredResultProm.then(
            $A.getCallback(function(filteredData) {
                component.set('v.targetRecordSet',filteredData);
                console.log(callback);
                if(callback) callback(filteredData);
                helper.doneLoading(component); 
            }),
            $A.getCallback(function(error) {
                console.error(error);
            })
        );
    },
    loadMore : function(component) {
        var helper = this;
        var className = component.get('v.dataProviderApiName');
        var filters = component.get('v.currentFilters');
        var resultLimit = component.get('v.loadLimit');
        resultLimit = resultLimit > 1 ? resultLimit : 20;
        var resultOffset = component.get('v.loadOffset')+resultLimit;
        var filterType = 'SERVER';
        var loadMoreProm = helper.retrieveFilteredRecordSetProm(component,filters,filterType,resultLimit,resultOffset);
        loadMoreProm.then($A.getCallback(function(filteredData) {
            var curData = component.get('v.targetRecordSet');
            curData.push(filteredData);
            console.log('loadMoreProm.then, filteredData is ');
            console.log(filteredData);
            component.set('v.targetRecordSet',curData);
            helper.doneLoading(component);
        }));
    },
    retrieveFilteredRecordSetProm : function(component,filters,filterType,resultLimit,resultOffset) {
        var helper = this;
        var retrieveFilteredResultProm;
        var className = component.get('v.dataProviderApiName');
        if(!$A.util.isUndefinedOrNull(filterType) && filterType.toUpperCase() === 'SERVER') {
            var url = window.location.href;
            var recordId = component.get('v.parentRecordUri');
            url += ((url.indexOf('?') > -1 ? '&' : '?') + 'parentRecordUri=' + recordId);
            retrieveFilteredResultProm = window.LADS.retrieveFilteredRecordSetByUrl(component,className,url,filters,resultLimit,resultOffset);
            component.set('v.currentFilters',filters);
            component.set('v.loadLimit',resultLimit);
            component.set('v.loadOffset',resultOffset);
        } else {
            retrieveFilteredResultProm = new Promise(function(resolve) {
                var data = component.get('v.initdata');
                
                var filteredData = [];
                if(!$A.util.isUndefinedOrNull(data) 
                   && !$A.util.isEmpty(data)){
                    for(var dataIndex = 0;dataIndex < data.length;dataIndex++) {
                        var item = data[dataIndex];
                        var isValid = true;
                        if(!$A.util.isUndefinedOrNull(filters) 
                           && !$A.util.isEmpty(filters)){
                            if(filters.length > 0) {
                                for (var filterIndex = 0; filterIndex < filters.length; filterIndex++) {
                                    //String,Integer,Date,DateTime,Decimal,Double,Long,etc. valid primitive type
                                    var filter = filters[filterIndex];
                                    if (!$A.util.isUndefinedOrNull(filter.valueType) && !$A.util.isUndefinedOrNull(filter.property) && !$A.util.isUndefinedOrNull(filter.operator)) {
                                        var val = filter.value;
                                        var operator = !$A.util.isUndefinedOrNull(filter.operator) ? filter.operator.toUpperCase() : '';
                                        
                                        
                                        switch (filter.valueType.toUpperCase()) {
                                                
                                            case 'STRING':
                                                isValid = helper.checkFilterStringData(item, filter);
                                                break;
                                            case 'BOOLEAN':
                                                isValid = helper.checkFilterBooleanData(item, filter);
                                                break;
                                            case 'INTEGER':
                                                isValid = helper.checkFilterNumberData(item, filter);
                                                break;
                                            case 'DATE':
                                                isValid = helper.checkFilterDateData(item, filter);
                                                break;
                                            case 'DATETIME':
                                                isValid = helper.checkFilterDateData(item, filter);
                                                break;
                                            case 'DECIMAL':
                                                isValid = helper.checkFilterNumberData(item, filter);
                                                break;
                                            case 'DOUBLE':
                                                isValid = helper.checkFilterNumberData(item, filter);
                                                break;
                                            default:
                                                isValid = helper.checkFilterStringData(item, filter);
                                                break;
                                        }
                                        
                                    }
                                }
                                if (isValid) filteredData.push(item);
                            } 
                        }else {
                            filteredData.push(item);
                        }
                    }
                }
                resolve(filteredData);
            });
        }
        return retrieveFilteredResultProm;
    },
    checkFilterStringData : function(item,filter) {
        var propValStr = String(item[filter.property]).toLowerCase(); //case insensitive
        var valStr = String(filter.value).toLowerCase(); //case insensitive
        switch(filter.operator.toUpperCase()) {
            case 'EQUALS':
                if(propValStr === valStr) return true;
                break;
            case 'NOTEQUALS':
                if(propValStr !== +valStr) return true;
                break;
            case 'LESSTHAN':
                if(propValStr < valStr) return true;
                break;
            case 'LESSTHANEQUALTO':
                if(propValStr <= valStr) return true;
                break;
            case 'GREATERTHAN':
                if(propValStr > valStr) return true;
                break;
            case 'GREATERTHANEQUALTO':
                if(propValStr >= valStr) return true;
                break;
            case 'CONTAINS':
                if(propValStr.indexOf(valStr) > -1) return true;
                break;
            case 'STARTSWITH':
                if(propValStr.startsWith(valStr)) return true;
                break;
            case 'ENDSWITH':
                if(propValStr.endsWith(valStr)) return true;
                break;
            default:
                //invalid operator
                break;
        }
        return false;
    },
    checkFilterNumberData : function(item,filter) {
        var propValNum = parseFloat(item[filter.property]);
        var valNum = parseFloat(filter.value);
        switch(filter.operator) {
            case 'EQUALS':
                if(propValNum === valNum) return true;
                break;
            case 'NOTEQUALS':
                if(propValNum !== +valNum) return true;
                break;
            case 'LESSTHAN':
                if(propValNum < valNum) return true;
                break;
            case 'LESSTHANEQUALTO':
                if(propValNum <= valNum) return true;
                break;
            case 'GREATERTHAN':
                if(propValNum > valNum) return true;
                break;
            case 'GREATERTHANEQUALTO':
                if(propValNum >= valNum) return true;
                break;
            default:
                //invalid operator
                break;
        }
        return false;
    },
    checkFilterBooleanData : function(item,filter) {
        var propValBool = Boolean(item[filter.property]);
        var valBool = Boolean(filter.value);
        switch(filter.operator) {
            case 'EQUALS':
                if(propValBool === valBool) return true;
                break;
            case 'NOTEQUALS':
                if(propValBool !== valBool) return true;
                break;
            default:
                //invalid operator
                break;
        }
        return false;
        
    },
    checkFilterDateData : function(item,filter) {
        var propValDt = new Date(item[filter.property]);
        var valDt = new Date(filter.value);
        switch(filter.operator) {
            case 'EQUALS':
                if(+propValDt === +valDt) return true;
                break;
            case 'NOTEQUALS':
                if(+propValDt !== +valDt) return true;
                break;
            case 'LESSTHAN':
                if(propValDt < valDt) return true;
                break;
            case 'LESSTHANEQUALTO':
                if(+propValDt <= +valDt) return true;
                break;
            case 'GREATERTHAN':
                if(propValDt > valDt) return true;
                break;
            case 'GREATERTHANEQUALTO':
                if(+propValDt >= +valDt) return true;
                break;
            default:
                //invalid operator
                break;
        }
    },
})