({
	onKeyUp : function(component, event, helper) {
        var stringVal = component.get('v.fieldName');
        component.set('v.fieldName',helper.getFormattedPhone(stringVal));        
    }
})