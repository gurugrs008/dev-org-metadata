({
	clickCreateItem : function(component, event, helper) {
        var validItem=true;
        var nameField=component.find("itemName");
        if($A.util.isEmpty(nameField)){
            validItem=false;
            nameField.set("v.errors",[{message:"Item name can't be left blank"}]);
        }
        else{
            nameField.set("v.errors",null);
        }
        var validQuantity=true;
        var quantityField=component.find("quantity");
        if($A.util.isEmpty(quantityField)){
            validQuantity=false;
            quantityField.set("v.errors",[{message:"Quantity can't be left blank"}]);
        }
        else{
         	quantityField.set("v.errors",null);	   
        }
        
		var validPrice=true;
        var priceField=component.find("price");
        if($A.util.isEmpty(priceField)){
            validprice=false;
            priceField.set("v.errors",[{message:"price can't be left blank"}]);
        }
        else{
         	priceField.set("v.errors",null);	   
        }
        
        if(validItem){
            var newItem = component.get("v.newItem");
            console.log("Create item: " + JSON.stringify(newItem));
            //helper.createItem(component, newItem);
            //        var theItems = component.get("v.items");
 
        // Copy the expense to a new object
        // THIS IS A DISGUSTING, TEMPORARY HACK
       /* var newItem = JSON.parse(JSON.stringify(item));
 
        console.log("Items before 'create': " + JSON.stringify(theItems));
		theExpenses.push(newItem);
		component.set("v.expenses", theItems);
		console.log("Items after 'create': " + JSON.stringify(theItems));
        theItems.push(newItem);
        component.set("v.items", theItems);
            
        component.set("v.newItem",{ 'sobjectType': 'Camping_Item__c','Name': '','Quantity__c': 0,
                    'Price__c': 0,'Packed__c': false });*/
            
           helper.createCamping(component, newItem);
            
        }
	},
    
    doInit: function(component, event, helper) {

    // Create the action
    var action = component.get("c.getItems");

    // Add callback behavior for when response is received
    action.setCallback(this, function(response) {
        var state = response.getState();
        if (component.isValid() && state === "SUCCESS") {
            component.set("v.items", response.getReturnValue());
        }
        else {
            console.log("Failed with state: " + state);
        }
    });

    // Send action off to be executed
    $A.enqueueAction(action);
  },
  handleAddItem: function(component, event, helper) {
    //   var newItem = event.getParam("item");
    //helper.addItem(component, newItem);
    var action = component.get("c.saveItem");
    		action.setParams({"item": newItem});
    		action.setCallback(this, function(response){
        		var state = response.getState();
        		if (component.isValid() && state === "SUCCESS") {
            		// all good, nothing to do.
            		var items = component.get("v.items");
            		items.push(response.getReturnValue());
            		component.set("v.items", items);
        		}
    		});
    		$A.enqueueAction(action);	
  },  
})