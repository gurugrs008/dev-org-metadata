({
	doInit : function(component, event, helper) {
        console.log('This is doInit of Modal');
    	var cls = component.get('v.class');
        if(cls != null && cls.length > 0) {
            var modal = component.find('test-modal');
            $A.util.addClass(modal,cls);
        }
        
    },
    
    toggleVisibility : function(component, event, helper) {
        var modal = component.find('test-modal');
        var visible = component.get('v.visible');
        visible = !visible;
        component.set('v.visible',visible);
        $A.util.toggleClass(modal,"slds-fade-in-open");
	},
})