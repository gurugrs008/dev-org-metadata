({
    doInit:function(component, event, helper) {
        var action = component.get("c.getCampingItems");
        action.setCallback(this, function(a) {
            component.set("v.item", a.getReturnValue());
        });
        $A.enqueueAction(action); 
    },	
	packItem:function(component, event, helper) {
		var btn = event.getSource();
        var btnMsg = btn.get("v.label");
        console.log("Event triggered from : " + btnMsg);
        //component.set("v.item", btnMsg);
        component.set("v.item.Packed__c", true);
        //btn.disabled=true;
        //btn.set("v.disabled",true);
        component.set("v.disableButton", true);
	}
})