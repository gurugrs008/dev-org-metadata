({    
    handleUploadFinished: function (cmp, event) {
        //Get the list of uploaded files
        var uploadedFiles = event.getParam("files");
        console.log('uploaded files == ');
        console.log(uploadedFiles);
        console.log(uploadedFiles[0]);
        var fileURL = component.get('v.fileURL');
        component.set('v.fileURL','https://gaurav-sharma-dev-ed.lightning.force.com/lightning/r/ContentDocument/'+fileURL);
        //Show success message – with no of files uploaded
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "type" : "success",
            "message": uploadedFiles.length+" files has been uploaded successfully!"
        });
        toastEvent.fire();
         
        $A.get('e.force:refreshView').fire();
         
        //Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    }
})