({
    doInit : function(component, event, helper) {
       	var action = component.get('c.retrieveRecord');
        action.setCallback(this,function(response){
            component.set('v.data',response.getReturnValue());            
        });
        $A.enqueueAction(action);
    },
    renderTile : function(component, event, helper) {                
        var data = component.get('v.data');        
        if(component.get('v.multipleValues') === true) {
            var compProms = [];
            compProms.push(helper.renderAttribute(component,component.get('v.valueType1'),data[component.get('v.property1')]));
            compProms.push(helper.renderAttribute(component,component.get('v.valueType2'),data[component.get('v.property2')]));
            compProms.push(helper.renderAttribute(component,component.get('v.valueType3'),data[component.get('v.property3')]));            
            Promise.all(compProms).then(function(promResults) {
                component.set('v.component1',promResults[0]);
                component.set('v.component2',promResults[1]);
                component.set('v.component3',promResults[2]);                
            });
        } else {
            helper.renderAttribute(component,component.get('v.valueType1'),data[component.get('v.property1')]).then(function(comp1){
                component.set('v.component1',comp1);
            });            
        }        		
	}
})