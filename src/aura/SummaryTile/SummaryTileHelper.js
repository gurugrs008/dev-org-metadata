({
	renderAttribute : function(component,attributeType,value) {
        var compProm = new Promise($A.getCallback(function(resolve,reject) {
            var compType = '';
            var attributes = {};
            var shouldRender = false;
            var attrType = attributeType.toLowerCase();
            switch(attrType) {
                    
                //Currency
                case 'currency':
                    compType = 'lightning:formattedNumber';
                    attributes.value = value;
                    attributes.style = 'currency';
                    shouldRender = true;
                    break;
                
                //Percent
                case 'percent':
                    compType = 'lightning:formattedNumber';
                    attributes.value = value;
                    attributes.style = 'percent';
                    shouldRender = true;
                    break; 
                    
                //decimal
                case 'decimal':
                    compType = 'lightning:formattedNumber';
                    attributes.value = value;
                    attributes.style = 'decimal';
                    shouldRender = true;
                    break;                 
                
                    //Date
                case 'date':
                    compType = 'lightning:formattedDateTime';
                    attributes.value = value;
                    shouldRender = true;                
                    break;
                       
                //Text
                case 'text':
                    compType = 'lightning:formattedText';
                    attributes.value = value;
                    attributes.title=value;
                    shouldRender = true;
                    break;
                   
                default:
                    
            }
            
            if(shouldRender === true) {
                $A.createComponent(
                    compType,
                    attributes,
                    function(newComp, status, errorMessage){
                        //Add the new button to the body array
                        if (status === "SUCCESS") {
                            resolve(newComp);
                        }
                        else if (status === "INCOMPLETE") {                            
                            console.log("No response from server or client is offline.")
                            // Show offline error
                        }
                        else if (status === "ERROR") {
                            console.log("Error: " + errorMessage);
                            // Show error message
                        }
                    }
                ); 
                
            }
            else {
                resolve({});
            }
        }));
        
        return compProm;
		
	}
})