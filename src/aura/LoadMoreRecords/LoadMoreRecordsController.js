({
	doInit : function(component, event, helper) {
        var action = component.get("c.retrieveContactList");        
        action.setParams({
            "cursor": component.get("v.offset")
        });        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var ctctList = response.getReturnValue();
                component.set('v.ctctList',ctctList);
				var offset = component.get('v.offset');
                offset += 5;
                component.set('v.offset',offset);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    loadMore : function(component, event, helper) {
        var offset = component.get('v.offset');
        var ctctList = component.get('v.ctctList');
        var tempContList = [];
        var action = component.get("c.retrieveContactList");        
        action.setParams({
            "cursor": component.get("v.offset")
        });        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                tempContList = response.getReturnValue();
                var i,j;
                for(i=0,j=offset;i<offset;i++,j++){
                    ctctList[j] = tempContList[i];
                }
                if(tempContList.length < 5){
                    var loadMoreRecDiv = component.find('loadMoreRecDiv'); 
                    $A.util.addClass(loadMoreRecDiv,'slds-hide');
                    var restoreRecords = component.find('restoreRecords');
                    $A.util.removeClass(restoreRecords,'slds-hide');
                }
                component.set('v.ctctList',ctctList);
                offset += 5;
                component.set('v.offset',offset);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);    
    },
    loadAll : function(component, event, helper) {
        var action = component.get("c.retrieveAllContacts");        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var ctctList = response.getReturnValue();
                component.set('v.ctctList',ctctList);
				var offset = component.get('v.offset');
                offset = 0;
                component.set('v.offset',offset);
                var loadMoreRecDiv = component.find('loadMoreRecDiv'); 
                var restoreRecords = component.find('restoreRecords'); 
                $A.util.addClass(loadMoreRecDiv,'slds-hide');
                $A.util.removeClass(restoreRecords,'slds-hide');
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    restore : function(component, event, helper) {
        var action = component.get("c.retrieveContactList");        
        action.setParams({
            "cursor": 0
        });        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var ctctList = response.getReturnValue();
                component.set('v.ctctList',ctctList);
				var offset = 0;
                offset += 5;
                component.set('v.offset',offset);
                var loadMoreRecDiv = component.find('loadMoreRecDiv'); 
                var restoreRecords = component.find('restoreRecords'); 
                $A.util.removeClass(loadMoreRecDiv,'slds-hide');
                $A.util.addClass(restoreRecords,'slds-hide');
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
})