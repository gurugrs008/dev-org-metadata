({
    onRecordSetDataChanged : function(component, event, helper) {
              
        var item = event.getParam('item');
        var actionType = event.getParam('action');
        var index = event.getParam('index');
        var dataProviderApiNameFromEvent = event.getParam('dataProviderApiName');
        var dataProviderApiName = component.get('v.dataProviderApiName');        
        var allCompBody = component.get('v.customListItemComponents');
        if(actionType === 'insert'){
        
        }else if(actionType === 'update'){

        }else if(actionType === 'delete'){
            if(!$A.util.isUndefinedOrNull(index)){
	        		if(index < allCompBody.length && dataProviderApiNameFromEvent === dataProviderApiName){
	                allCompBody.splice(index,1);
	                
	                //Rearrange index for all remaining items
	                for(var i = 0 ; i < allCompBody.length ; i++){
	                    var itemComponent = allCompBody[i];
	                    itemComponent.set("v.itemIndex", i);
	                    allCompBody[i] = itemComponent;
	                }
	                component.set('v.customListItemComponents',allCompBody);
	                component.set('v.listCount', allCompBody.length);
	                helper.setRenderVals(component);
	            }
            
            } else if(!$A.util.isUndefinedOrNull(item)){
	        		if(dataProviderApiNameFromEvent === dataProviderApiName){
	        			var indexFromID;
	        			//Found index of deleted item
    	                for(var i = 0 ; i < allCompBody.length ; i++){
    	                    var itemComponent = allCompBody[i];
    	                    var itemInComp = itemComponent.get("v.item");
    	                    if(itemInComp.id === item.id){
    	                    		indexFromID = i;
    	                    }
    	                }
    	                
    	                if(!$A.util.isUndefinedOrNull(indexFromID)){
    	                		allCompBody.splice(indexFromID,1);
    		                //Rearrange index for all remaining items
    		                for(var i = 0 ; i < allCompBody.length ; i++){
    		                    var itemComponent = allCompBody[i];
    		                    itemComponent.set("v.itemIndex", i);
    		                    allCompBody[i] = itemComponent;
    		                }
    		                component.set('v.customListItemComponents',allCompBody);
    		                component.set('v.listCount', allCompBody.length);
    		                helper.setRenderVals(component);
    		                component.set("v.forceRefresh", true);
    	                }
	    			}
	        }
        }
    },
    handleUpdateListItem : function(component, event, helper) {
        var item = event.getParam('item');
        var actionType = event.getParam('actionType');
        var index = event.getParam('index');
        var dataProviderApiNameFromEvent = event.getParam('dataProviderApiName');
        var dataProviderApiName = component.get('v.dataProviderApiName');
        
        var allCompBody = component.get('v.customListItemComponents');
        if(actionType === 'Save'){
        
        }else if(actionType === 'Delete'){
			if(index < allCompBody.length && dataProviderApiNameFromEvent === dataProviderApiName){
                allCompBody.splice(index,1);
                
                //Rearrange index for all remaining items
                for(var i = 0 ; i < allCompBody.length ; i++){
                    var itemComponent = allCompBody[i];
                    itemComponent.set("v.itemIndex", i);
                    allCompBody[i] = itemComponent;
                }
                component.set('v.customListItemComponents',allCompBody);
                component.set('v.listCount', allCompBody.length);
                helper.setRenderVals(component);
            }
        }
    },
    
    addItem : function(component, event, helper) {
             
    	var allCompBody = [];
        allCompBody = component.get('v.customListItemComponents');
        var allowAddNewItem = true;//false;
        var toastEvent = $A.get("e.force:showToast");
        var maxListSize = component.get('v.maxListSize');
        if(!($A.util.isEmpty(maxListSize) || maxListSize === 0) && (!$A.util.isUndefinedOrNull(maxListSize) 
                                                                   && !$A.util.isEmpty(maxListSize))){
            if(allCompBody.length >= maxListSize){

                toastEvent.setParams({
                    "title": "Error",
                    "message": "You cannot add more items to this list."
                });
                toastEvent.fire();
                allowAddNewItem = false;

                return;
            }
        }

        if(allowAddNewItem){
            helper.toggleButtonLoading(component,true); 
            var itemNew = {};
            
            var compType = component.get('v.itemComponentTag');
            var props = {};
            props.item = itemNew;
            props.itemIndex = allCompBody.length;
            if(component.get('v.showNewEntriesatTop')){
                props.itemIndex = 0;            	
            }
            props.editable = true;
            props.editMode = true;
            props.dataProviderApiName = component.get('v.dataProviderApiName');
            props.inBuilder = component.get('v.inBuilder');
            $A.createComponent(compType,props,function(cmp,status,error) {
                if(component.get('v.showNewEntriesatTop')){
                    allCompBody.unshift(cmp);  
                    //Rearrange index for all remaining items if new item added on first pos
                    for(var i = 0 ; i < allCompBody.length ; i++){
                        var itemComponent = allCompBody[i];
                        itemComponent.set("v.itemIndex", i);
                        allCompBody[i] = itemComponent;
                    }
                }else{
                    allCompBody.push(cmp);
                }
                component.set('v.customListItemComponents', allCompBody);
                component.set('v.listCount', allCompBody.length);
                helper.toggleButtonLoading(component,false); 
                helper.setRenderVals(component);
            });
        }else{

            toastEvent.setParams({
                "title": "Error",
                "message": "Please save old record first."
            });
            toastEvent.fire();
            return;
        }
    },
    handleStateChange : function(component, event, helper) {
        var oldVal = event.getParam("oldValue");
        var newVal = event.getParam("value");
        if(newVal.indexOf('/expanded/') > -1) {
            helper.expandCollapseList(component);
        }

    },
    toggleExpanded : function(component, event, helper) {
        helper.expandCollapseList(component);
    },
    loadMore : function(component,event,helper) {
        helper.loadNextPage(component);
    },
    handleDataContextChange : function(component,event,helper) {

        var contextProperty = component.get('v.canManagePropertyName');
        var contextRecord = component.get('v.context');
        var hasContext = !$A.util.isUndefinedOrNull(contextProperty) && contextProperty.length > 0 ;
        var contextSet = !hasContext || (!$A.util.isUndefinedOrNull(contextRecord) && !$A.util.isUndefinedOrNull(contextRecord[contextProperty]));
        var canManage = component.get('v.canManageRecordSet');

        if(!canManage && hasContext && contextSet) component.set('v.canManageRecordSet', contextRecord[contextProperty]);
        var data = component.get('v.data');
        var dataSet = !$A.util.isUndefinedOrNull(data);
        var count = component.get('v.listCount');
        
        if(count != data.length) {
            //data has changed...
            component.set('v.listCount', data.length);
            
        }
        if(data.length <= component.get("v.currentLimit")) {
            component.set('v.enableLoadMore',false);
        }
        //refresh component if both data and context are available
        if(contextSet && dataSet) {
            helper.refreshComponent(component);
        }
    }

})