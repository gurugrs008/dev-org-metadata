({
    setRenderVals : function(component) {
        
        //required param values
        var listCount = component.get('v.listCount');
        var canManage = component.get('v.canManageRecordSet');
        var hideBlank = component.get('v.hideWhenBlank');
        var isValid = component.get('v.isValid');
        var inBuilder = component.get('v.inBuilder');
        var isReadOnly = component.get('v.isReadOnly');
        var maxListSize = component.get('v.maxListSize');
        var isCollapsible = component.get('v.isCollapsible'); 
        var hideFromAdminWhenBlank = component.get('v.hideFromAdminWhenBlnk');
        
        //default collapsible class - TODO: Move to custom label / setting
        if(isCollapsible) component.set('v.collapseClass', 'collapse-content');

        //should render
        var shouldRender = (listCount > 0 || canManage || !hideBlank) && isValid && !(hideFromAdminWhenBlank && listCount <= 0);
        console.log('Should Render?',shouldRender,listCount,hideBlank,isValid);
        //show errors in builder
        var showError = inBuilder && !isValid;

        component.set('v.shouldRender',shouldRender);
        component.set('v.showError',showError);


        var canAdd = !isReadOnly && canManage && ($A.util.isUndefinedOrNull(maxListSize) || maxListSize <= 0 || (maxListSize > 0 && maxListSize > listCount));
        component.set('v.canAdd',canAdd);
        
    },
    toggleButtonLoading : function(component,showLoading) {
        var headerAddBtn = component.find('header-add-btn');
        var footerAddBtn = component.find('footer-add-btn');
        if(showLoading) {
            
            $A.util.addClass(headerAddBtn,'loading');
            
            $A.util.addClass(footerAddBtn,'loading');        
        } else {
           
            $A.util.removeClass(headerAddBtn,'loading');
        
            $A.util.removeClass(footerAddBtn,'loading');              
        }
        
    },
    initializeComponents : function(component) {
        //hide load more if less data than limit returned
        var helper = this;
        if(ret.length < component.get("v.currentLimit")) {
            component.set('v.enableLoadMore',false);
        }
        component.set('v.isListExpanded',!component.get('v.collapsedOnLoad')); 
        component.get("v.forceRefresh", false);

        helper.createDynamicComponents(component);
    }, 
    expandCollapseList : function(component) {
        var helper = this;
        var checked = component.get('v.isListExpanded');

        checked = !checked;
        component.set('v.isListExpanded',checked);
        var hasExpanded = component.get('v.hasExpanded');

        
        if(!hasExpanded) {
            hasExpanded = true;
            component.set('v.hasExpanded',hasExpanded);
        }
        
        var hasRendered = component.get('v.hasRenderedComponents');
        if(!hasRendered) {
            var dataToRender = component.get("v.dataToRender");
            //helper.createDynamicComponents(component,dataToRender);
            helper.renderComponent(component);
        }        
    },
    loadNextPage : function(component) {
        var helper = this;
        component.set("v.isLoadingMoreRows",true);
        //code added by Zibing to fix GLOBAL SEARCH BUG, code updated by Gaurav to fix Load More Records bug
        component.set("v.loadOffset", component.get('v.data').length);
        component.set("v.loadLimit",component.get('v.currentLimit'));
        //End code added by Zibing

        var numRows = 20; //how to set?
        console.log('component.get("v.data").length == '+component.get("v.data").length);
        if(component.get('v.isFirstLoadMore')){
        	component.set('v.items',component.get("v.data"));
        	component.set('v.isFirstLoadMore',false);
        }
        var items = component.get("v.items");
        console.log('Items.length ============ '+items.length);
        var curOffset = component.get('v.currentOffset');
        var maxCount = component.get('v.currentLimit');
        var curLimit = curOffset+maxCount;
        var itemsToRender = items.slice(0,curLimit);   
        component.set('v.isNewData', true);
        component.set('v.forceCollapseLargeLists',false);
        component.set('v.data',itemsToRender);
        //component.set('v.currentLimit',curLimit);
        helper.renderComponent(component);
        component.set('v.isLoadingMoreRows',false);
        if(curLimit >= items.length){
        	component.set('v.enableLoadMore',false);     
        }

        
    },
    renderComponent : function(component,refresh) {
        var helper = this;
        var isNewData = component.get('v.isNewData');            
        var hasRun = component.get('v.hasRenderedComponents');
        
        //@todo only rerender new or changed items and remove deleted ones
        var loadMoreEnabled = component.get('v.enableLoadMore');
        var maxCount = component.get('v.currentLimit');
        var items = component.get("v.data");
        var isFirstLoadMore = component.get('v.isFirstLoadMore');
        var maxAddCount = component.get("v.maxListSize"); //this only affects adding items


        // @todo unlikely to actually align with Lightning limit in practice
        var compsToGen = [];
        var max = (items.length > maxCount && loadMoreEnabled && isFirstLoadMore ? maxCount : items.length);
        var isReadOnly = component.get('v.isReadOnly');
        var canManageRecordSet = component.get('v.canManageRecordSet');
        var isEditable = (canManageRecordSet && !isReadOnly);
        var collapsible = component.get('v.isCollapsible');
        var expanded = component.get('v.isListExpanded');
        var hasExpanded = component.get('v.hasExpanded');
        

        //properties needed to create list item components dynamically
        var componentTag = component.get('v.itemComponentTag');
        var activeStates = component.get('v.activeStates');
        var dataProviderApiName = component.get('v.dataProviderApiName');
        var inBuilder = component.get('v.inBuilder');
        var uniqueName = component.get('v.componentUniqueName');
        
        var asyncLoadingComp = component.find('asyncLoadingStatus');
        var loadingComp = component.find('listLoadingStatus');        

        var forceCollapseLargeLists = component.get('v.forceCollapseLargeLists');
        var autoCollapseMin = component.get('v.autoCollapseMin');        
        $A.util.removeClass(asyncLoadingComp,'hideMe');
            
        //generate a component for each item in list within max
        //pass default LADS properties
        if(items.length > 0) {
            for(var i = 0; i < max; i++){
                var compToGen = [];
                compToGen.push(componentTag);
                var props = {};
                props.item = items[i];
                props.dataProviderApiName = dataProviderApiName;
                props.itemIndex = i;
                props.editable = isEditable;
                props.inBuilder = inBuilder;
                props.parentComponentUniqueName = uniqueName;
                props.activeStates = activeStates;
                compToGen.push(props);
                compsToGen.push(compToGen);
            }
        }
        
        //Render list if non-empty
        if(compsToGen.length > 0) {
            var customListItemComponents = [];
            $A.createComponents(compsToGen, function (cmps, status, error) {
                if (status === "SUCCESS") {
                    var allCompBody = cmps;              
                    component.set('v.customListItemComponents', allCompBody);
                    component.set('v.listCount', allCompBody.length);                        
                    component.set('v.hasRenderedComponents',true);
                    component.set('v.isValid', true);
                    component.set('v.isEmpty', false); 
                }
                else if (status === "INCOMPLETE") {
                    // @todo should be a custom label or pass error if populated
                    console.warn("Incomplete - No response from server or client is offline.")                       
                }
                else if (status === "ERROR") {
                    console.error(error);
                    component.set('v.isValid', false);
                    //@todo populate error message in builder from error response
                }

                //hide loading/stencils once done
                $A.util.addClass(loadingComp,'slds-hide');
                $A.util.addClass(asyncLoadingComp, 'hideMe');
                helper.setRenderVals(component);
                
            });
            var currentOffset = component.get('v.currentOffset');
            var currentLimit = component.get('v.currentLimit');
            currentOffset += currentLimit;
            component.set('v.currentOffset',currentOffset);
        }

        //List is empty
        else {

            component.set('v.isEmpty', true);
            component.set('v.isValid', true);
            
            //hide loading/stencils once done
            $A.util.addClass(loadingComp,'slds-hide');
            $A.util.addClass(asyncLoadingComp, 'hideMe');            
            component.set('v.enableLoadMore',false);                        
            helper.setRenderVals(component);
            

        } 

    },    
    refreshComponent : function(component) {

        var helper = this;
        var isNewData = component.get('v.isNewData');            
        var hasRun = component.get('v.hasRenderedComponents');
        
        //@todo only rerender new or changed items and remove deleted ones
        var items = component.get("v.data");
        var maxCount = component.get("v.maxListSize");
        // @todo unlikely to actually align with Lightning limit in practice
        maxCount = ((!$A.util.isEmpty(maxCount) && maxCount > 0) ? maxCount : 10000); 
        var compsToGen = [];
        var max = (items.length > maxCount ? maxCount : items.length);
        var isReadOnly = component.get('v.isReadOnly');
        var canManageRecordSet = component.get('v.canManageRecordSet');
        var isEditable = (canManageRecordSet && !isReadOnly);
        var collapsible = component.get('v.isCollapsible');
        var expanded = component.get('v.isListExpanded');
        var hasExpanded = component.get('v.hasExpanded');
       

        //properties needed to create list item components dynamically
        var componentTag = component.get('v.itemComponentTag');
        var activeStates = component.get('v.activeStates');
        var dataProviderApiName = component.get('v.dataProviderApiName');
        var inBuilder = component.get('v.inBuilder');
        var uniqueName = component.get('v.componentUniqueName');
        
        var asyncLoadingComp = component.find('asyncLoadingStatus');
        var loadingComp = component.find('listLoadingStatus');        

        var forceCollapseLargeLists = component.get('v.forceCollapseLargeLists');
        var autoCollapseMin = component.get('v.autoCollapseMin');

        //collapse if larger than min amount and auto-collapse is enabled
        if(autoCollapseMin > -1 && items.length>autoCollapseMin && forceCollapseLargeLists == true){
                component.set('v.isCollapsible',true);
                component.set('v.collapsedOnLoad',true);
        }

        // @todo What is this snippet for?

        //only render if expanded or non-collapsible
        //isNewData = loading via loadNextPage
        //hasRun = has rendered components yet
        if((isNewData || !hasRun) && ((collapsible && expanded) || !collapsible)) {
            
            //show indicator during rendering
            
            helper.renderComponent(component);               
        }
        //Should not render currently
        else{
            console.log('not rendering '+dataProviderApiName);
            console.log(items.length);
            //hide loading/stencils once done
            $A.util.addClass(loadingComp,'slds-hide');
            $A.util.addClass(asyncLoadingComp, 'hideMe');
            helper.setRenderVals(component);
            
        }        
    }
    	
})