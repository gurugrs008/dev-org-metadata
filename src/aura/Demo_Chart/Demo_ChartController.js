({  
    doInit : function(component, event, helper){        
        var action = component.get('c.getDivisionToColorMapping');
        action.setCallback(this, function(response){
            component.set('v.divisionColorMappings', response.getReturnValue());  
            var colorMappings = component.get('v.divisionColorMappings');
        });
        $A.enqueueAction(action);               
        
        var action1 = component.get('c.retrieveRecordSet');
        action1.setCallback(this, function(response){
            component.set('v.data', response.getReturnValue());
            helper.renderChart(component);
        });
        $A.enqueueAction(action1);
      
    }
})