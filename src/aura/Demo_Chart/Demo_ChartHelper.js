({
    renderChart : function(component) {
        var dataResult =   component.get('v.data');
        var defaultColors = ['#61B7ED','#FF9365','#A094EE','#13C3B7','#E5645B','#50ECFC','#FFCA42','#C83AA5','#D4ED62','#53688C'];
        var colorData = component.get('v.divisionColorMappings');
        //resetting defaultColors array from metadata records if found
        try{
            var item = {}; // an empty object
            var opts = {};
            try {
                var optsTemp = component.get('v.options');
                if(optsTemp !== null && typeof optsTemp == 'object') {
                    opts = optsTemp;                    
                } else {
                    var optsStr = String(component.get('v.options')).trim();
                    if(optsStr.indexOf('\'') > -1) optsStr = optsStr.split('\'').join('\"');
                    if(!optsStr.startsWith('{')) optsStr = '{' + optsStr;
                    if(!optsStr.endsWith('}')) optsStr = optsStr + '}';
                    opts = JSON.parse(optsStr);
                }

            } catch(e) {
                console.warn(e);
            }
            if(!opts.tooltips) {
                opts.tooltips = {};
                opts.tooltips.callbacks = {};
            }
            if(!opts.legend) {
                opts.legend = {};    
            }
            
            if($A.get("$Browser.formFactor") == "DESKTOP") {
                opts.responsive = true;
                if(!opts.legend.position) opts.legend.position = 'right';
            } else {
                opts.responsive = true;
                if(!opts.legend.position) opts.legend.position = 'bottom';
            }
            console.log(JSON.stringify(opts));
            var chartType = component.get('v.chartType').toLowerCase();
            var totalAmount = 0;
            var resultLabels = [];
            // Changes made as per  I-329436
            for(var ob in dataResult){
                for(var obj in dataResult[ob].data){
                    totalAmount += dataResult[ob].data[obj];
                }
            }
            // an empty array of division labels
            var allLabels = [];
            for(var ob in dataResult){
                for(var obj in dataResult[ob].labels){
                    allLabels.push(dataResult[ob].labels[obj]);
                    var resLabel = dataResult[ob].labels[obj] + (chartType != 'pie' && chartType != 'donut' ? '-' + (parseFloat( dataResult[ob].data[obj]/ totalAmount * 100).toFixed(2)) + '%' : '');
                    resultLabels.push(resLabel);
                }
            }

            opts.tooltips.callbacks.label = function(tooltipItem, data) {
                var label = '';
                try {
                    if($A.util.isUndefinedOrNull(tooltipItem.datasetIndex)) tooltipItem.datasetIndex = 0;
                    label = data.labels[tooltipItem.index];
                    var amount = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    var amountFormatted = parseFloat(amount).toFixed(2).replace(/./g, function(c, i, a) {
                        return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
                    });
                    //pie
                    if(chartType == 'pie' || chartType == 'donut') {


                        var total = 0;
                        for(var i = 0;i<data.datasets[tooltipItem.datasetIndex].data.length;i++) {
                            total += data.datasets[tooltipItem.datasetIndex].data[i]
                        }


                        if(label) {
                            label += ' : ';
                        }
                        label += (parseFloat(amount / total * 100).toFixed(2) + '% ($'+amountFormatted+')');
                    }
                    if(chartType == 'line') {
                        label += ' ('+data.datasets[tooltipItem.datasetIndex].label+') ';
                        label += ' : $'+amountFormatted;
                    }
                    if(chartType == 'bar') {
                        label = '';
                        label += data.datasets[tooltipItem.datasetIndex].label;
                        label += ' : $'+amountFormatted;
                    }

                } catch(e) {
                    console.warn(e);
                }
                return label;
            };
            /*opts.legend.onHover = function(e,legendItem){
                debugger;
                
            }*/
            /*opts.tooltips.callbacks.afterLabel = function(tooltipItem, data){
                var label='After LAbel';
                return label;
            }*/
            item.datasets = [];// an emtpy array
            item.labels = [];
            var data = [];
            var hasMultiSources = component.get('v.hasMultipleSources');
            if(hasMultiSources) {
                item = component.get('v.multiData');

	          for(var i = 0;i<item.datasets.length;i++) {
	            item.datasets[i].stack = i;
	          }

            } else {
                for(var ob in dataResult){
                    var numItems = 0;
                    try {
                        numItems = dataResult[ob].data.length;
                    } catch(e) {
                        console.warn(e);
                    }

                    var bgColor = defaultColors.slice(0,numItems);
                    // Logic to map color with designation as per defined in metadata setting
                    for(var labelIndx in allLabels){
                        if(!$A.util.isEmpty(colorData) && !$A.util.isEmpty(colorData[allLabels[labelIndx]])){
                            bgColor[labelIndx] = colorData[allLabels[labelIndx]];
                        }
                    }
					item.datasets.push({
                        'data' : dataResult[ob].data,
                        'backgroundColor' : bgColor
                    });
                    if(!$A.util.isEmpty(resultLabels)){
                        item.labels = resultLabels;
                    }else{
                        item.labels = dataResult[ob].labels;
                    }
                }
            }

            component.set('v.item',item);
            var el = component.find('chart_lifetime').getElement();
            var ctx = el.getContext('2d');
            ctx.width = component.get('v.width');
            ctx.height = component.get('v.height');
            var renderedChart = new Chart(ctx,{
                type: component.get('v.chartType').toLowerCase(),
                data: item,
                options: opts

            });
            if(item.datasets[0].data.length == 0) {
                component.set('v.hasData',false);
                $A.util.addClass(component.find('chart-container'),'slds-hide');
            } else {
                component.set('v.hasData',true);
            }           
        }catch(e){
            console.error(e);
        }
    }
})