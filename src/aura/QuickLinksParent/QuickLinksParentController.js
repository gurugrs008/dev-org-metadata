({
	addQuickLink : function(component, event, helper) {
		var allCompBody = component.get('v.customListItemComponents');
        var itemNew = {};            
        var compType = 'c:QuickLinks';
        var props = {};
        props.item = itemNew;
        props.itemIndex = allCompBody.length;
        props.editable = true;
        props.editMode = true;
        $A.createComponent(compType,props,function(cmp,status,error) {
            component.set('v.customListItemComponents', allCompBody);
            component.set('v.listCount', allCompBody.length);
        });
	}
})