({

    handleBlurField : function(component, event, helper) {
        var data = component.get('v.data');
        var val = data[component.get('v.propertyName')];
        var stringVal = component.get('v.stringFieldValue'); 
        if(!$A.util.isEmpty(stringVal)) {
            helper.unRenderPristineClass(component);
        } else {
            helper.renderPristineClass(component);
        }
    },
    onDataChange : function(component, event, helper) {
        helper.handleComponentRendering(component);
    },
    validateConfig : function(component, event, helper) {
        //validate proper input provided
        //populate error messages and set isValid to false
    },
    reRenderFieldValue : function(component, event, helper) {
        var contextApiName = component.get('v.contextApiName');
        var data = component.get('v.data');
        if(!$A.util.isUndefinedOrNull(contextApiName)) component.set('v.canManageRecord',data[contextApiName]);
        var val = data[component.get('v.propertyName')];
        var compType = component.get('v.componentType');
        if(!$A.util.isUndefinedOrNull(val)) {
            component.set('v.isBlank',false);
            if(compType.toLowerCase() === 'text' || compType.toLowerCase() === 'textarea' || compType.toLowerCase() === 'richtext' || compType.toLowerCase() === 'picklist' || compType.toLowerCase() === 'url') {
                component.set('v.stringFieldValue',val);
                component.set('v.lastSavedStringFieldValue',val);
                if(compType.toLowerCase() === 'url'){
                  var URLval = data[component.get('v.PropertyNameURL')];
                  component.set('v.stringFieldValue2',URLval);
                  component.set('v.lastSavedStringFieldValue2',URLval);
                }
            } else if(compType.toLowerCase() === 'date' || compType.toLowerCase() === 'datetime') {
                try {
                    component.set('v.dateFieldValue', new Date(val).toISOString().substr(0, 10));
                } catch(e) {

                }
            } else if(compType.toLowerCase() === 'boolean') {
                component.set('v.booleanFieldValue', val);
                component.set('v.stringFieldValue', val); //Use for block type Boolean
            } else if(compType.toLowerCase() === 'currency'){
                component.set('v.currencyFieldValue',val);
            }
        } else {
            component.set('v.isBlank',false);
            helper.renderPristineClass(component);

        }
        helper.renderInlineInputComponent(component,data);
    },
    handleInlineFieldEvent : function(component, event, helper) {
        var params = event.getParams();
        component.set('v.readOnlyField',params.readOnlyField);
        if(params.refreshData === true) {
            helper.initializeComponent(component, event,true);
        }
    },
    reRenderComponent : function(component,event, helper) {
        if(event.getParam('componentId') === component.getGlobalId()) {
            var comp = event.getParam('selectedComponent');
            helper.renderInlineInputComponent(component);
        }
    },
    toggleListExpanded : function(component, event, helper) {
        helper.toggleListExpanded(component, event, helper);
    },
    toggleEdit : function(component, event, helper) {
        var fieldStyle = component.get('v.fieldStyle');
        if(fieldStyle == 'Inline') {
            helper.toggleInlineFieldEdit(component, event, helper);
        } else if(fieldStyle == 'Block') {
            helper.toggleBlockFieldEdit(component, event, helper);
        }
    },
    saveBlockField : function(component, event, helper) {


        var stringValue = component.get('v.stringFieldValue');
        var compType = component.get("v.componentType");
        var maxLength = component.get("v.maxLength");
        if( compType === 'RichText' && !$A.util.isUndefinedOrNull(maxLength)) {
            if(stringValue.length > maxLength) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error",
                    "message": "Sorry, you have reach the limit for characters for the field. We apologize for the inconvenience.",
                    "type" : "error"
                });
                toastEvent.fire();
                return;
            }
        }
        var fdata = component.get('v.data');
        var prop = component.get('v.propertyName');
        var URLProp = component.get('v.PropertyNameURL');
        fdata[prop] = stringValue;
        component.set('v.lastSavedStringFieldValue',stringValue);
        var isValid = true;
        if(compType === 'URL'){
            // Check validity for url inputs
            var urlNameValidity = component.find("personalWebsiteName").get("v.validity");
            var urlLinkValidity = component.find("personalWebsiteUrl").get("v.validity");
            if(!urlNameValidity.valid
               || !urlLinkValidity.valid){
                isValid = false;
            }
        }

        if(!$A.util.isUndefinedOrNull(URLProp)) {
            var stringValue2 = component.get('v.stringFieldValue2');
            fdata[URLProp] = stringValue2;
            component.set('v.lastSavedStringFieldValue2',stringValue2);
        }
        if(isValid){
            helper.synchronizeData(component,fdata);
            var editBlock = component.find('block-field-edit-block');
            var readBlock = component.find('block-field-read-block');
            $A.util.toggleClass(editBlock,'slds-hide');
            $A.util.toggleClass(readBlock,'slds-hide');
        }
    },
    handleCancelClick : function(component,event,helper) {
        //for URL call remove
        //otherwise clear field and save and toggle edit
        var compType = component.get("v.componentType");

        if(compType === 'URL'){
            helper.removeBlockFieldForURL(component,event,helper);
        } else {
            var fdata = component.get('v.data');
            var prop = component.get('v.propertyName');
            
            var stringFieldValue = '';
            

            component.set('v.stringFieldValue', '');
            
            component.set('v.isBlank', true);

            component.set('v.lastSavedStringFieldValue', stringFieldValue);
            

            fdata[prop] = stringFieldValue;
            

            helper.synchronizeData(component,fdata);

            var fieldStyle = component.get('v.fieldStyle');
            if(fieldStyle == 'Inline') {
                helper.toggleInlineFieldEdit(component, event, helper);
            } else if(fieldStyle == 'Block') {
                helper.toggleBlockFieldEdit(component, event, helper);
            }
        }
        helper.renderPristineClass(component);
    },

    cancelInlineComponentEdit : function(component, event, helper) {

        var lastSavedStringFieldValue = component.get('v.lastSavedStringFieldValue');

        if(!$A.util.isUndefinedOrNull(lastSavedStringFieldValue)){
            component.set('v.stringFieldValue', lastSavedStringFieldValue);
        }else{
            component.set('v.stringFieldValue', '');

        }
        var compType = component.get('v.componentType');
        if(compType.toLowerCase() === 'picklist') {
            helper.toggleInlineSelectEdit(component);
        } else {
            var inlineInput = component.find('inline-input-field');
            $A.util.removeClass(inlineInput, 'unsaved');
        }
    },
    saveInlineComponent : function(component, event, helper) {
        var stringFieldValue = component.get('v.stringFieldValue');
        component.set('v.lastSavedStringFieldValue', stringFieldValue);
        var compType = component.get("v.componentType");
        var maxLength = component.get("v.maxLength");
        var stringValue = component.get('v.stringFieldValue');
        if( compType === 'RichText' && !$A.util.isUndefinedOrNull(maxLength)) {
            if(stringValue.length > maxLength) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error",
                    "message": "Sorry, you have reach the limit for characters for the field. We apologize for the inconvenience.",
                    "type" : "error"
                });
                toastEvent.fire();
                return;
            }
        }
        var fdata = component.get('v.data');
        var prop = component.get('v.propertyName');

        component.set('v.lastSavedStringFieldValue',stringValue);

        fdata[prop] = stringValue;

        helper.synchronizeData(component,fdata);
        if(compType.toLowerCase() === 'picklist') {
            helper.toggleInlineSelectEdit(component);
        } else {
            var inlineInput = component.find('inline-input-field');
            $A.util.removeClass(inlineInput, 'unsaved');
        }
    },
    editInlineComponent : function(component, event, helper) {
        var inlineInput = component.find('inline-input-field');
        $A.util.addClass(inlineInput,'unsaved');
    },
    toggleDescription : function(component, event, helper) {
        event.stopPropagation();
    },
    toggleBlockFieldEdit : function(component, event, helper) {
        helper.toggleBlockFieldEdit(component, event, helper);
    },
    toggleInlineFieldEdit : function(component, event, helper) {
        helper.toggleInlineFieldEdit(component, event, helper);
    },
})