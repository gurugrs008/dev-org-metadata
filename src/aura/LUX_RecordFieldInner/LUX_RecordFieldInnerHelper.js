({
    unRenderPristineClass : function(component) {
        var compType = component.get('v.componentType');
        if(compType.toLowerCase() === 'text' || compType.toLowerCase() === 'textarea' || compType.toLowerCase() === 'currency') {
            var inlineInput = component.find('inline-input-field');
            $A.util.removeClass(inlineInput,'pristine');
            
            var blockInput = component.find('block-field-read-block');
            $A.util.removeClass(blockInput,'pristine');
            
        } else if(compType.toLowerCase() === 'richtext') {
            var blockDiv = component.find('block-field-read-block');
            $A.util.removeClass(blockDiv,'pristine');
        }
    },
    renderPristineClass : function(component) {
        var compType = component.get('v.componentType');
        if(compType.toLowerCase() === 'text' || compType.toLowerCase() === 'textarea' || compType.toLowerCase() === 'currency') {
            var inlineInput = component.find('inline-input-field');
            $A.util.addClass(inlineInput,'pristine');
            
            var blockInput = component.find('block-field-read-block');
            $A.util.addClass(blockInput,'pristine');
            
        } else if(compType.toLowerCase() === 'richtext') {
            var blockDiv = component.find('block-field-read-block');
            $A.util.addClass(blockDiv,'pristine');
        }
    },
    renderInlineInputComponent : function(component, data) {
        //@todo implement this rendering if necessary
        var contextApiName = component.get('v.contextApiName');
        var visibilityApiName = component.get('v.visibilityApiName');
        if(!$A.util.isEmpty(contextApiName)) component.set('v.canManageRecord',data[contextApiName]);
        if(!$A.util.isEmpty(visibilityApiName)) component.set('v.hideOnPersona',!data[visibilityApiName]);
        
        //render inline dynamic component
        try{
            var showInlineFieldComp = component.get('v.showDynamicInlineComp');
            var props = {};
            var inlineComponentParams = component.get('v.inlineComponentParams');
            if(!$A.util.isEmpty(inlineComponentParams)){
                props = JSON.parse(inlineComponentParams);
            }
            var compsToGen = [];
            var compToGen = [];
            var compName = component.get('v.dynamicInlineCompName');
            compToGen.push(compName);
            props.data = component.get('v.data');
            props.dataProviderApiName = component.get('v.dataProviderApiName');
            props.readOnlyField = component.get('v.readOnlyField');
            props.propertyName = component.get('v.propertyName');
            props.componentType = component.get('v.componentType');
            props.inBuilder = component.get('v.inBuilder');
            
            props.groupName = props.data.id + '_' + props.propertyName;
            
            compToGen.push(props);
            compsToGen.push(compToGen);
            if(compsToGen.length > 0 && showInlineFieldComp === true) {
                $A.createComponents(compsToGen, function (cmps, status, error) {
                    if(status === 'SUCCESS') {
                    		component.set('v.dynamicInlineComp',cmps);
                    }
                });
            }
        }catch(ex){
            
        }
    },
    
    toggleListExpanded : function(component, event, helper) {
        var checked = component.get('v.isListExpanded');
        component.set('v.isListExpanded',!checked);
    },
    toggleInlineSelectEdit : function (component) {
        component.set('v.selectIsUnsaved',!component.get('v.selectIsUnsaved'));
    },
    handleComponentRendering : function(component) {
        var helper = this;
        var data = component.get('v.data');
        
        var compType = component.get('v.componentType');
        var contextApiName = component.get('v.contextApiName');
        var visibilityApiName = component.get('v.visibilityApiName');
        
        if(!$A.util.isUndefinedOrNull(contextApiName)) component.set('v.canManageRecord',data[contextApiName]);
        if(!$A.util.isUndefinedOrNull(visibilityApiName)) component.set('v.hideOnPersona',!data[visibilityApiName]);
        
        var val = data[component.get('v.propertyName')];
        if(!$A.util.isEmpty(val)) {
            component.set('v.isBlank',false);
            
            if(compType.toLowerCase() === 'text' || compType.toLowerCase() === 'textarea' || compType.toLowerCase() === 'richtext' || compType.toLowerCase() === 'picklist' || compType.toLowerCase() === 'url') {
                component.set('v.stringFieldValue',val);
            } else if(compType.toLowerCase() === 'date' || compType.toLowerCase() === 'datetime') {
                try {
                    component.set('v.dateFieldValue', new Date(val).toISOString().substr(0, 10));
                } catch(e) {
                    
                }
            } else if(compType.toLowerCase() === 'boolean') {
                // This is incomplete now as Inline css dosen't support Boolean. need to work on it
                component.set('v.booleanFieldValue', val);
                component.set('v.stringFieldValue', val); //Use for block type Boolean
            } else if(compType.toLowerCase() === 'currency'){
                
                component.set('v.currencyFieldValue',val);
            }
            helper.unRenderPristineClass(component);
        } else {
            component.set('v.isBlank',true);
            if(compType.toLowerCase() === 'text' || compType.toLowerCase() === 'textarea' || compType.toLowerCase() === 'richtext' || compType.toLowerCase() === 'currency') {
                var inlineInput = component.find('inline-input-field');
                $A.util.addClass(inlineInput,'pristine');
            }
            helper.renderPristineClass(component);
        }
        var propertyNameURL = component.get("v.PropertyNameURL");
        
        component.set('v.lastSavedStringFieldValue',component.get("v.stringFieldValue"));
        if(!$A.util.isUndefinedOrNull(propertyNameURL)) {
            component.set('v.stringFieldValue2',data[component.get("v.PropertyNameURL")]);
            component.set('v.lastSavedStringFieldValue2',component.get("v.stringFieldValue2"));
        }
        
      //render inline dynamic component
        try{
            var showInlineFieldComp = component.get('v.showDynamicInlineComp');
            var props = {};
            var inlineComponentParams = component.get('v.inlineComponentParams');
            if(!$A.util.isEmpty(inlineComponentParams)){
                props = JSON.parse(inlineComponentParams);
            }
            var compsToGen = [];
            var compToGen = [];
            var compName = component.get('v.dynamicInlineCompName');
            compToGen.push(compName);
            props.data = component.get('v.data');
            props.dataProviderApiName = component.get('v.dataProviderApiName');
            props.readOnlyField = component.get('v.readOnlyField');
            props.propertyName = component.get('v.propertyName');
            props.componentType = component.get('v.componentType');
            props.inBuilder = component.get('v.inBuilder');
            
            props.groupName = props.data.id + '_' + props.propertyName;
            
            compToGen.push(props);
            compsToGen.push(compToGen);
            if(compsToGen.length > 0 && showInlineFieldComp === true) {
                $A.createComponents(compsToGen, function (cmps, status, error) {
                    if(status === 'SUCCESS'){ 
                    		component.set('v.dynamicInlineComp',cmps);
                		}
                });
            }
        }catch(ex){
            
        }
        
    },
    synchronizeData : function(component,data, event) {
        if(component.isValid()) {
            var recordProvider = component.find('data-provider');
            recordProvider.syncData();
            //how to know error?
            var isError = false;            
            if(!isError) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success",
                    "message": "Saved Successfully",
                    "type" : "success"
                });
                toastEvent.fire();
            }
            else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error",
                    "message": ret[0].message,
                    "type" : "error"
                });
                toastEvent.fire();
            }            
        }
    },
    removeBlockFieldForURL : function(component, event, helper) {
        var fdata = component.get('v.data');
        var prop = component.get('v.propertyName');
        var URLProp = component.get('v.PropertyNameURL');
        var stringFieldValue = '';
        var stringFieldValue2 = '';
        
        component.set('v.stringFieldValue', '');
        component.set('v.stringFieldValue2', '');
        component.set('v.isBlank', true);
        
        component.set('v.lastSavedStringFieldValue', stringFieldValue);
        component.set('v.lastSavedStringFieldValue2', stringFieldValue2);
        
        fdata[prop] = stringFieldValue;
        fdata[URLProp] = stringFieldValue2;
        
        helper.synchronizeData(component,fdata);
        
        var editBlock = component.find('block-field-edit-block');
        var readBlock = component.find('block-field-read-block');
        $A.util.toggleClass(editBlock,'slds-hide');
        $A.util.toggleClass(readBlock,'slds-hide');
        
    },
    toggleInlineFieldEdit : function(component, event, helper) {
        var compType = component.get('v.componentType');
        if(compType.toLowerCase() === 'picklist') {
            
            helper.toggleInlineSelectEdit(component);
        } else {
            var inlineInput = component.find('inline-input-field');
            $A.util.toggleClass(inlineInput, 'unsaved');
        }
    },
    toggleBlockFieldEdit : function(component, event, helper) {
        var hasRendered = component.get('v.hasRenderedEdit');
        if(hasRendered !== true) {
            component.set('v.hasRenderedEdit',true);
        }
        var compType = component.get('v.componentType');
        if(compType.toLowerCase() != 'boolean') {
            var lastSavedStringFieldValue = component.get('v.lastSavedStringFieldValue');
            var lastSavedStringFieldValue2 = component.get('v.lastSavedStringFieldValue2');
            if(!$A.util.isUndefinedOrNull(lastSavedStringFieldValue)){
                component.set('v.stringFieldValue', lastSavedStringFieldValue);
            }else{
                component.set('v.stringFieldValue', '');
                
            }
            if(!$A.util.isUndefinedOrNull(lastSavedStringFieldValue2)){
                component.set('v.stringFieldValue2', lastSavedStringFieldValue2);
            }else{
                component.set('v.stringFieldValue2', '');
                
            }
        }
        var editBlock = component.find('block-field-edit-block');
        var readBlock = component.find('block-field-read-block');
        $A.util.toggleClass(editBlock,'slds-hide');
        $A.util.toggleClass(readBlock,'slds-hide');
    },        
})