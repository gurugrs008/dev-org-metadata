({
	click:function(cmp, event, helper){
     var firstName = cmp.get('v.firstName');
     if($A.util.isUndefinedOrNull(firstName)){
         alert('Hi, I am invoked in parent component on click of button from child component mark up. Please enter a name to display here');
     }else{
         alert('Hi, I am invoked in parent component on click of button from child component mark up. The name you entered is '+firstName);
     }
   },
   clickAgain:function(cmp, event, helper){
     var firstName = cmp.get('v.firstName');
     if($A.util.isUndefinedOrNull(firstName)){
         alert('Hi, I am invoked in parent component on click of button from child component JS controller. Please enter a name to display here');
     }else{
         alert('Hi, I am invoked in parent component on click of button from child component JS controller. The name you entered is '+firstName);
     }
   }
})