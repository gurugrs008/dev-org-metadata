({
    getMessage : function(component, event) {
        var params = event.getParam('arguments');
        if (params) {
            var param1 = params.Name;
            return "##### Hello "+param1+" From Child Component #####";
        }
        return "";
    },
    methodToFireEvent : function(component, event, helper) {
        console.log('inside method of child');
        var compEvent = component.getEvent("addressValidationEvent");
        compEvent.setParams({
            "street1" : "Mahindra Sez Street"
        });
        compEvent.fire();
    }
})