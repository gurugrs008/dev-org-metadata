({
	getOptions : function(component) {
        var value = component.get("v.value");
        var defaultValueforStart = null;
        var isvalue = true;
        var getNoneOption = component.get('v.noneOptionLabel');
        if($A.util.isEmpty(getNoneOption)){
            getNoneOption = '--None--';
        }
		var opts = component.get("v.options");
        var cmp = component.find('selectInput');
        cmp.set('v.body', []); // clear all options
        var body = cmp.get('v.body');
        
        if($A.util.isUndefinedOrNull(value)) {
            isvalue = false; 
            $A.createComponent(
                'aura:html',
                {
                    tag: 'option',
                    HTMLAttributes: {
                        value: null,
                        //text: '--None--',
                        text:getNoneOption,
                        selected : 'selected'
                    }
                },
                function (newOption) {
                    //Add options to the body
                    if (component.isValid()) {
                        
                        body.push(newOption);
                    }
                });
            
        }
        else {
            $A.createComponent(
                'aura:html',
                {
                    tag: 'option',
                    HTMLAttributes: {
                        value: null,
                        //text: '--None--',
                        text:getNoneOption,
                        selected : ''
                    }
                },
                function (newOption) {
                    //Add options to the body
                    if (component.isValid()) {
                        
                        body.push(newOption);
                    }
                });
        }
        
        opts.forEach(function (opt) {
            if(opt === value) {
                defaultValueforStart = opt;
                $A.createComponent(
                    'aura:html',
                    {
                        tag: 'option',
                        HTMLAttributes: {
                            value: opt,
                            text: opt,
                            selected: 'selected'
                        }
                    },
                    
                    function (newOption) {
                        //Add options to the body
                        if (component.isValid()) {
                            body.push(newOption);
                            cmp.set('v.body', body);
                        }
                    })
            }
            else {
            	$A.createComponent(
                    'aura:html',
                    {
                        tag: 'option',
                        HTMLAttributes: {
                            value: opt,
                            text: opt
                        }
                    },
                    
                    function (newOption) {
                        //Add options to the body
                        if (component.isValid()) {
                            body.push(newOption);
                            cmp.set('v.body', body);
                        }
                    })     
            }
        });
       cmp.set('v.value',defaultValueforStart);
    }
})