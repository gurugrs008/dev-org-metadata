({
    doInit : function(component, event, helper) {
        var options = component.get('v.options');
        var optionProviderAPIName = component.get('v.optionProviderAPIName');
        // Get picklist options if not defined static options
        if($A.util.isUndefinedOrNull(options) || options.length === 0){
            // Get picklist options from object and field api name if optionProviderAPIName is not defined
            if($A.util.isUndefinedOrNull(optionProviderAPIName) || $A.util.isEmpty(optionProviderAPIName)){
                var retrieveOptionsAction = component.get("c.retrievePicklistFieldOptions");
                retrieveOptionsAction.setParams({objectName : component.get("v.objectAPIName"), 
                                                 fieldName : component.get("v.fieldAPIName")});
                retrieveOptionsAction.setCallback(this,function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var optionsArr = response.getReturnValue();
                        component.set("v.options", optionsArr);
                        //helper.getOptions(component);
                    }			
                });
                $A.enqueueAction(retrieveOptionsAction);
            }
            // Get picklist options from optionProviderAPIName class
            else{
                // Need to call method once have logic in ctrl and have interface defined
            }
        }
        else{
            // Need to call method once have logic in ctrl and have interface defined
            //helper.getOptions(component);
        }
    }
})