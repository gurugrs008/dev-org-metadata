({
	doInit : function(component, event, helper) {
        var action = component.get("c.retrieveContactList");                     
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var ctctList = response.getReturnValue();
                component.set('v.ctctList',ctctList);				
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    selectListView : function(component, event, helper) {
        var listView = component.find('list-view');
        var gridView = component.find('grid-view');        
        $A.util.addClass(gridView,'slds-hide');
        $A.util.removeClass(listView,'slds-hide');

        var gridBtn = component.find('grid-btn');
        var listBtn = component.find('list-btn');

        $A.util.addClass(listBtn,'btn-primary');
        $A.util.removeClass(listBtn,'btn-default');

        $A.util.removeClass(gridBtn,'btn-primary');
        $A.util.addClass(gridBtn,'btn-default');        
    },
    selectGridView : function(component, event, helper) {
        var listView = component.find('list-view');
        var gridView = component.find('grid-view');        
        $A.util.addClass(listView,'slds-hide');
        $A.util.removeClass(gridView,'slds-hide');

        var gridBtn = component.find('grid-btn');
        var listBtn = component.find('list-btn');

        $A.util.addClass(gridBtn,'btn-primary');
        $A.util.removeClass(gridBtn,'btn-default');

        $A.util.removeClass(listBtn,'btn-primary');
        $A.util.addClass(listBtn,'btn-default');          
    }
})