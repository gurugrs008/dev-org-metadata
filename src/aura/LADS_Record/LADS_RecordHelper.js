({
    recordDataChanged : function(component,data){
        component.set('v.targetRecord',data);
    },

    retrieveInitializeComponentPromise : function(component){
        var url = window.location.href;
        var recordId = component.get('v.recordUri');
        url += ((url.indexOf('?') > -1 ? '&' : '?') + 'recordUri=' + recordId);
        var className = component.get('v.dataProviderApiName');
        try {
            //return window.LADS.retrieveRecordByUrl(component,className,url);
        } catch(e) {
            
        }		
    },
    initializeComponent : function(component,callback){
        var helper = this;
        
        var url = window.location.href;
        var recordId = component.get('v.recordUri');
        url += ((url.indexOf('?') > -1 ? '&' : '?') + 'recordUri=' + recordId);
        var className = component.get('v.dataProviderApiName');
        var forceRefresh = component.get('v.forceRefresh');
        var useBg = component.get('v.fetchInBackground');
        /*window.LADS.retrieveRecordByUrlCB(component,className,url,forceRefresh,useBg,function(ret) { 
            console.log(ret);
            
            component.set('v.targetRecord', ret);
        });*/
    },
    retrieveSynchronizeDataPromise : function(component,data) {
        var className = component.get('v.dataProviderApiName');
        try {
            return window.LADS.saveRecord(component,className,data);		
        } catch(e) {
        }		
    },
    synchronizeData : function(component,callback) {
        var data;
        if(component.isValid()) {
            console.log('syncing in helper 1');
            var helper = this;
            if($A.util.isUndefinedOrNull(data)) {
                data = component.get('v.targetRecord');
            }
            var className = component.get('v.dataProviderApiName');
            try {
                window.LADS.saveRecordCB(component,className,data,$A.getCallback(function(res) {

                    var data = res;
                    //fire event to notify others and update their data
                    var recordDataChangedEvent = $A.get('e.c:LADS_RecordDataChanged');
                    recordDataChangedEvent.setParams({
                        "identifier" : component.getGlobalId(),
                        "action" : "update",
                        "dataProviderApiName" : className,
                        "data" : data
                    });

                    recordDataChangedEvent.fire();
                    
                    if(callback) {
                        callback(true);
                    }
                }));
            } catch(errMsg) {
                    if(callback) {
                        callback(false);
                    }
            }
        }
    },
    startLoading : function(component) {
        this.toggleLoading(component);
    },
    stopLoading : function(component) {
        this.toggleLoading(component);
    },  
    toggleLoading : function(component) {
        
    },
    
})