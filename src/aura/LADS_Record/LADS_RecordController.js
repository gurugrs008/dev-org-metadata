({
	doInit : function(component, event, helper) {	
		var shouldBatch = component.get('v.batchRequests');
		if(!shouldBatch) { 
			helper.initializeComponent(component);
		}
		else {
        	
        	var appEvent = $A.get("e.c:LADS_DataRequest");
            appEvent.setParams({
                "dataProviderApiName" : component.get('v.dataProviderApiName'),
                "providerType" : "Record",
                "useBackground" : component.get('v.fetchInBackground')
            });
            appEvent.fire();
        }		
	},
    handleDataResponseEvent : function(component, event, helper) {
    	var params = event.getParams();
        Object.keys(params.recordData).forEach(function(key) {
        	if(key == component.get('v.dataProviderApiName')){
        		component.set('v.targetRecord',params.recordData[key]);
        		//handle component rendering
                
        	}
        });
        
    },	
	onRecordDataChanged : function(component, event, helper) {
		//should update?
		var identifier = event.getParam('identifier');
		var action = event.getParam('action');
		var dataProviderApiName = event.getParam('dataProviderApiName');
		var data = event.getParam('data');
		
        console.log(action,identifier,component.getGlobalId(),dataProviderApiName,component.get('v.dataProviderApiName'),data);
		if(action === 'update' 
			&& identifier !== component.getGlobalId() 
			&& component.get('v.dataProviderApiName') === dataProviderApiName) 
		{
			console.log('same?',data == component.get('v.targetRecord'),data.workPhone,component.get('v.targetRecord').workPhone);
            if(data != component.get('v.targetRecord')) {
                helper.recordDataChanged(component,data);
            }
		}

	},
	onSyncData : function(component, event, helper) {
        var params = event.getParam('arguments');
        var callback;
        if (params) {
            callback = params.callback;            
        }		
		helper.synchronizeData(component,callback);
	},
    
    reloadRecord : function(component, event, helper) {	
		helper.initializeComponent(component);
	},
	handleLocationChange : function(component, event, helper) {
		var params = event.getParams();
		var token = event.getParam("token");
		var qs = event.getParam("querystring");
	},
	onToggleLoading : function(component, event, helper) {
		helper.toggleLoading(component);
	}
})