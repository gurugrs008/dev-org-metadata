({
    doInit : function(component, event, helper) {
        console.log('This is doInit of parent');
        var modal = component.find('testModal');
        $A.util.addClass(modal,'slds-hide');
    },
    
    toggleModal : function(component, event, helper) {
        var modal = component.find('testModal');
        modal.toggleVisibility();
    }
})